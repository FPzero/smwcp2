db $42
JMP Main : JMP Main : JMP Main : JMP Return : JMP Return : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

	!Screen = $07	;Screen Number.
	!Level = $01C1	;Level Number
	!ELevel = $01C0	;External Level Number.
	!Tile = $03C0	;Tile of that block

Main:
	PHY		;Preserve Y
	JSR Button
	JSR Code	;Jump to our code.
	JSR Map16
Jumpp:
	PLY		;Pull back Y
Return:
	RTL		;Return.

Button:
	LDA $16
	AND #$08
	BEQ Ret
	RTS
Ret:
	PLA
	PLA
	JMP Jumpp

Code:
	LDX #!Screen
	LDY $19D8,x	;Start with properties.
	REP #$20	;Make the Accumulator 16-bit.
	LDA $03
	CMP.w #!Tile
	BEQ First
	LDA #!ELevel
	BRA ExCh
First:
	LDA #!Level	;Load the level to Accumulator.
ExCh:
	PHA
	XBA		;Swap B and A.
	SEP #$20	;Go back to 8-bit mode for A.
	CMP #$01	;Is the level starting with 01...
	BNE Skip	;...if it is, skip setting bit.
	TYA		;Transfer Y to A, A now has properties.
	ORA #$01	;Set the bit 0.
	BRA Jump	;Jump to store properties.
Skip:
	TYA		;Transfer Y to A, A now has properties.
	AND #$FE	;Clear bit 0.
Jump:
	STA $19D8,x	;Store the properties.
	REP #$20	;Go to 16-bit mode for A.
	PLA		;Load level number.
	SEP #$20	;Keep only the lower bit.
	STA $19B8,x	;Store it to exit table.
	TYA		;Transfer Y to A, A now has properties.
	AND #$F5	;Clear various bits.
	STA $19D8,x	;Store those properties.
Sub:
	RTS		;Go back to our wrapper.

Map16:
	REP #$20
	LDA $03
	CMP.w #!Tile
	BNE Back
	CLC
	ADC.w #$0001
	BRA Store
Back:
	SEC
	SBC.w #$0001
Store:
	STA $03
	JSR MAP16CHANGE
	SEP #$30
	LDA #$0B
	STA $1DF9
	RTS

MAP16CHANGE:
			PHP
			REP #$30
			PHY
			PHX
			TAX
			LDA $03
			PHA
			JSR SUB_8034
			PLA
			STA $03
			PLX
			PLY
			PLP
			RTS

RETURN18:		PLX
			PLB
			PLP
			RTS

SUB_8034:		PHP
			SEP #$20
			PHB
			LDA #$00
			PHA
			PLB
			REP #$30
			PHX
			LDA $9A
			STA $0C
			LDA $98
			STA $0E
			LDA #$0000
			SEP #$20
			LDA $5B
			STA $09
			LDA $1933
			BEQ NO_SHIFT
			LSR $09
NO_SHIFT:		LDY $0E
			LDA $09
			AND #$01
			BEQ HORIZ
			LDA $9B
			STA $00
			LDA $99
			STA $9B
			LDA $00
			STA $99
			LDY $0C
HORIZ:			CPY #$0200
			BCS RETURN18
			LDA $1933
			ASL A
			TAX
			LDA $BEA8,x
			STA $65
			LDA $BEA9,x
			STA $66
			STZ $67
			LDA $1925
			ASL A
			TAY
			LDA ($65),y
			STA $04
			INY
			LDA ($65),y
			STA $05
			STZ $06
			LDA $9B
			STA $07
			ASL A
			CLC
			ADC $07
			TAY
			LDA ($04),y
			STA $6B
			STA $6E
			INY
			LDA ($04),y
			STA $6C
			STA $6F
			LDA #$7E
			STA $6D
			INC A
			STA $70
			LDA $09
			AND #$01
			BEQ NO_AND
			LDA $99
			LSR A
			LDA $9B
			AND #$01
			BRA LABEL52
NO_AND:			LDA $9B
			LSR A
			LDA $99
LABEL52:		ROL A
			ASL A
			ASL A
			ORA #$20
			STA $04
			CPX #$0000
			BEQ NO_ADD
			CLC
			ADC #$10
			STA $04
NO_ADD:			LDA $98
			AND #$F0
			CLC
			ASL A
			ROL A
			STA $05
			ROL A
			AND #$03
			ORA $04
			STA $06
			LDA $9A
			AND #$F0
			LSR A
			LSR A
			LSR A
			STA $04
			LDA $05
			AND #$C0
			ORA $04
			STA $07
			REP #$20
			LDA $09
			AND #$0001
			BNE LABEL51
			LDA $1A
			SEC
			SBC #$0080
			TAX
			LDY $1C
			LDA $1933
			BEQ LABEL50
			LDX $1E
			LDA $20
			SEC
			SBC #$0080
			TAY
			BRA LABEL50
LABEL51:		LDX $1A
			LDA $1C
			SEC
			SBC #$0080
			TAY
			LDA $1933
			BEQ LABEL50
			LDA $1E
			SEC
			SBC #$0080
			TAX
			LDY $20
LABEL50:		STX $08
			STY $0A
			LDA $98
			AND #$01F0
			STA $04
			LDA $9A
			LSR A
			LSR A
			LSR A
			LSR A
			AND #$000F
			ORA $04
			TAY
			PLA
			SEP #$20
			STA [$6B],y
			XBA
			STA [$6E],y
			XBA
			REP #$20
			ASL A
			TAY
			PHK
			PER MAP16_RETURN-$01
			PEA $804C
			JML $00C0FB
MAP16_RETURN:		PLB
			PLP
			RTS