;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Musical Bounce Block
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

db $42

JMP MarioAbove : JMP MarioAbove : JMP Mario : JMP Mario : JMP Mario : JMP Mario : JMP Mario

MarioAbove:	LDA #$A0	;Load a value for the speed, 00-7F = Down, 80-FF = Up. 7F and 80 are the max values.
		STA $7D		;Place this value into Mario's Speed.
MarioSide:	LDA $77
		AND #$04
		BNE Mario
		LDA #$29	;Load the sound value you want to play.
		STA $1DFC	;I/O Port played (see http://www.smwcentral.net/?p=viewthread&t=6665)
Mario:		RTL		;Return
