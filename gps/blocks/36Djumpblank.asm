;Act like a blank tile if Jumped block. If Mario jumps again, it will act like the muncher block.
;Use it with levelASM
;Acts like = 25

db $42
JMP Start : JMP Start : JMP Side : JMP Start : JMP Start : JMP Return : JMP Start : JMP Start : JMP Start : JMP Start

Start:
LDA $14AF
BNE DoSomething
LDY #$01
LDA #$30
STA $1693
BRA Return
DoSomething:
LDY #$01
LDA #$2F
STA $1693
BRA Return
Side:
LDA $14AF
BNE Lorceee
LDY #$01
LDA #$30
STA $1693
BRA Return
Lorceee:
LDY #$01
LDA #$30
STA $1693
Return:
RTL
