header
lorom

;!Freespace = $1C8000;Edit this if you don't want your ROM to crash

org $80800A 
autoclean JML Main

freecode

Main:
STZ $2140
STZ $2141
STZ $2142
STZ $2143
CLC
XCE
SEP #$FF
LDA #$80
STA $2100
REP #$28

STZ $2181
LDY.b #$7E
STY $2183
LDA.w #$8008
STA $4300
LDA.w #NullByte
STA $4302
LDY.b #NullByte/65536
STY $4304
STZ $4305
LDY #$01
STY $420B

STZ $2181
LDY.b #$7F
STY $2183
LDA.w #$8008
STA $4300
LDA.w #NullByte
STA $4302
LDY.b #NullByte/65536
STY $4304
STZ $4305
LDY #$01
STY $420B

SEP #$20
JML $808010

NullByte:
db $00
