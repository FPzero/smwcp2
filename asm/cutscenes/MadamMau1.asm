SetSpeakerToMadamMau:
	LDX !madamMau1Index
	STX !currentSpeaker
	RTL

MadamMau1Create:
	REP #$20
	LDA.w #MadamMau1INIT
	STA $00
	LDX.b #MadamMau1INIT>>16
	STX $02
	
	LDA.w #MadamMau1MAIN
	STA $03
	LDX.b #MadamMau1MAIN>>16
	STX $05
	
	LDA.w #MadamMau1NMI
	STA $06
	LDX.b #MadamMau1NMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !madamMau1Index
RTL

MadamMau1Delete:
	LDX !madamMau1Index
	JSL DeleteActor
RTL

MadamMau1INIT:

	RTL

MadamMau1MAIN:
	PHK
	PLB
	
	LDA #$78		; \
	STA $00			; / X pos
	
	LDA #$3F		; \ Y pos
	STA $01			; /
	
	LDA !textIsAppearing	; If there is no text appearing,
	BNE .openMouth		;
	LDA #$40		; Then draw tile $40
	BRA +			;
.openMouth			;
	LDA $13			; \
	AND #$0C		; |
	LSR			; | Otherwise, animate the mouth.
	CLC			; |
	ADC #$40		; /
	
+
	STA $02
	
	LDA #$3C		; \
	STA $03			; / Property
	
	;STX $04
	LDA #$02
	JSL DrawTile
	

	
	LDA #$2F		; \ Y pos
	STA $01			; /
	
	LDA #$2C		; \
	STA $02			; / Tile
	
	;INC $04			; Index
	
	LDA #$02		; Size
	
	JSL DrawTile		;
	
	
	LDA #$4F		; \ Y pos
	STA $01			; /
	
	LDA #$0C		; \
	STA $02			; / Tile
	
	;INC $04			; Index
	
	LDA #$02		; Size
	
	JSL DrawTile		;
	
	RTL
MadamMau1NMI:
;	LDA !actorVar1
;	BNE +	
;	STA $7FFFD1
;	REP #$10
;	LDX #!layer1MapAddr>>1			; | 
;	LDY #!CS1DesertTilemapExGFXSize		; |
;	JSR UploadFromBuffer
;	SEP #$30
;	
;	;LDA.b #!layer1MapAddr+$800>>11<<2	; \
;	;STA $2107				; /
;	LDA #$01
;	STA !actorVar1
;	+
	RTL
;
;MadamMau1DecompressTileset:
;	REP #$30
;	LDA #!CS1DesertTilemapExGFX		; \
;	JSL DecompressGFXFile			; /
;	SEP #$30
;	RTL
