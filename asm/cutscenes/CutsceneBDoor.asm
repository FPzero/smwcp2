

!cBDoorXPosStatic = $78
!cBDoorYPosStatic = $35
!cBDoorAnimTime = $28


!cBDoorState = !actorVar1,x		; 0 = opening, 1 = closing, 2 = still
;!cBDoorTimer = !actorVar2,x
!cBDoorBlacknessHeight = !actorVar3,x	

CutsceneBDoorClose:
	LDX !cBDoorIndex
	LDA #!cBDoorAnimTime
	STA !cBDoorBlacknessHeight
	
	LDA #$01
	STA !cBDoorState
	RTL
	
CutsceneBDoorOpen:
	LDX !cBDoorIndex
	LDA #$00
	STA !cBDoorBlacknessHeight
	
	LDA #$00
	STA !cBDoorState
	RTL

	
CutsceneBDoorCreate:
	REP #$20
	LDA.w #CutsceneBDoorINIT
	STA $00
	LDX.b #CutsceneBDoorINIT>>16
	STX $02
	
	LDA.w #CutsceneBDoorMAIN
	STA $03
	LDX.b #CutsceneBDoorMAIN>>16
	STX $05
	
	LDA.w #CutsceneBDoorNMI
	STA $06
	LDX.b #CutsceneBDoorNMI>>16
	STX $08
	SEP #$20
	JSL NewActor
	STX !cBDoorIndex
	RTL

CutsceneBDoorINIT:
	LDX !cBDoorIndex
	LDA #$02
	STA !cBDoorState
	RTL

CutsceneBDoorMAIN:
	
	PHK
	PLB

	LDA !cBDoorState
	CMP #$02
	BEQ CutsceneBDoorDraw
	CMP #$00
	BEQ CutsceneBDoorOpening
	BRA CutsceneBDoorClosing

CutsceneBDoorNMI:
	RTL
	

CutsceneBDoorClosing:
	LDA !cBDoorBlacknessHeight
	DEC
	BMI CutsceneBDoorDraw
	STA !cBDoorBlacknessHeight
	BRA CutsceneBDoorDraw
	

CutsceneBDoorOpening:
	LDA !cBDoorBlacknessHeight
	INC
	CMP #!cBDoorAnimTime
	BCS CutsceneBDoorDraw
	STA !cBDoorBlacknessHeight
	;BRA CutsceneBDoorDraw


CutsceneBDoorDraw:
	LDA #$00 : STA $02
	LDA #$31 : STA $03
	
	
	
	LDA #$06
	STA $02
	
	LDY #$0E
-	
	LDA CBBlacknessXPositions,y
	CLC : ADC #!cBDoorXPosStatic
	STA $00
	
	LDA CBBlacknessYPositions,y
	CLC : ADC #!cBDoorYPosStatic
	SEC : SBC !cBDoorBlacknessHeight
	STA $01
	
	LDA #$02 : JSL DrawTile
	
	DEY
	BPL -

	

	LDA #$00 : STA $02
	LDA #!cBDoorXPosStatic :                 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$18 : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$10 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$08 : STA $01
	LDA #$02 : JSL DrawTile
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$18 : STA $00
	LDA #!cBDoorYPosStatic                   : STA $01
	LDA #$02 : JSL DrawTile
	
	
	
	
	
	LDA #$02 : STA $02

	LDA #!cBDoorXPosStatic : CLC : ADC #$10 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$18 : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$18 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$18 : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$18 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$10 : STA $01
	LDA #$02 : JSL DrawTile
	
	
	
	
	
	LDA #$04 : STA $02

	LDA #!cBDoorXPosStatic : CLC : ADC #$28 : STA $00
	LDA #!cBDoorYPosStatic                   : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$38 : STA $00
	LDA #!cBDoorYPosStatic                   : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$28 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$10 : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$38 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$10 : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$28 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$18 : STA $01
	LDA #$02 : JSL DrawTile

	LDA #!cBDoorXPosStatic : CLC : ADC #$38 : STA $00
	LDA #!cBDoorYPosStatic : CLC : ADC #$18 : STA $01
	LDA #$02 : JSL DrawTile
	
	
	
	RTL

CBBlacknessXPositions:
db $00, $10, $20, $30, $38
db $00, $10, $20, $30, $38
db $00, $10, $20, $30, $38

CBBlacknessYPositions:
db $00, $00, $00, $00, $00
db $10, $10, $10, $10, $10
db $18, $18, $18, $18, $18
	

