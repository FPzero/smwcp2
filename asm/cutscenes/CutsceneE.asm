!BowserSpriteCHR = 	$0A25
!KamekSpriteCHR = 	$0A26

!CutsceneEBackgroundCHR1 = $0247
!CutsceneEBackgroundCHR2 = $0102
!CutsceneEForegroundTilemap = $0A35
!CutsceneEBackgroundTilemap = $0A36

CutsceneEPalette:
incbin CutsceneE.rawpal
	
CSINITE:
	STZ $2121
	%StandardDMA(#$2122, CutsceneEPalette, #512, #$00) 
	
	REP #$30	
	LDA #!BowserSpriteCHR			; |
	LDX #$C000>>1				; | Upload Bowser
	LDY #!CS1DocCrocExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	REP #$30
	LDA #!KamekSpriteCHR			; \ 
	LDX #$D000>>1				; | Upload SP2
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneEBackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneEBackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneEForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!CutsceneEBackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	SEP #$30
	
	LDA #$14
	STA !backgroundColor
	LDA #$18
	STA !backgroundColor+1
	LDA #$18
	STA !backgroundColor+2
	
	REP #$20
	LDA #$00D8				; \ Layer 1 X
	STA $1A					; /
	LDA #$00E2				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0060				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	
	LDA #$00
	STA !currentFont
	
	JSL BowserChairCreate
	JSL BowserCreate
	JSL KamekCreate
	JSL MarioCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$53 : JSL MarioSetTargetX

	JSL BowserSetToRelaxing
	JSL SetSpeakerToKamek
	
	RTL

	

CSET1:
db $F4
db "And there's the drink you ordered, Your Burliness.", $EF, $EB
%VWFASM(SetSpeakerToBowser)
db "I gotta say, the Doc was right about this whole vacation thing! Those constant battles with Mario really take a lot out on a guy!", $EF, $EB
%VWFASM(SetSpeakerToKamek)
db "It goes without saying that someone as magnificently evil such as yourself deserves a break every once in a while.", $EF, $EB
%VWFASM(SetSpeakerToBowser)
db "And this view--", $EE, $F6, "couldn't be better! I should totally build an"
%VWFASM(KamekSetToFrantic)
db " awesome new resort castle here! ", $EF, $EB
%VWFASM(SetSpeakerToKamek)
db "Er, sire?", $EB
%VWFASM(SetSpeakerToBowser)
db "Gwa ha ha! It'd be perfect! Vacations whenever I want_", $EF, $EB
%VWFASM(SetSpeakerToKamek)
db "Excuse me, sire?", $EB
%VWFASM(SetSpeakerToBowser)
db "No bumbling servents getting in the way_", $EF, $EB
%VWFASM(SetSpeakerToKamek)
db "Sire, you might want to know that--", $EB
%VWFASM(SetSpeakerToBowser)
db "And best of all, no annoying interruptions from--", $EE
%VWFASM(MarioSetToVarWalkRight)
db $F8
%VWFASM(BowserJump)
%VWFASM(BowserSetToSquashKamek)
%VWFASM(BowserSetToNormal)
db "MARIO!"
db $F4
db $FF, $EF, $EB
db "Well look who it is. I can't even enjoy a simple vacation without you butting in, can I?", $EF, $EB
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db $FF
db "Factories, what?", $EF, $EB
db "What are you even talking about? I deal in princess kidnapping! In particular one specific princess. And she's not here!", $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db $F8
db "Of course not! I don't do factories! I have way more awesome things on my agenda than chopping down trees or whatever!", $EF, $EB
%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db $FF
db "What? My minions? That's why you're here?", $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db $F8
%VWFASM(BowserSetToWavingArms)
db "Gwa ha ha! I deserve this awesome vacation, but those twerps sure don't! "
%VWFASM(BowserSetToNormalArms)
db "Some green-skinned guy with stupid hair and crazy glasses offered me a ton of money and this vacation to borrow them for a while! "
db "<Construction, security, and damage prevention> or some nonsense like that. I don't care--", $EE, "I got the better end of the deal!", $EF, $EB
%VWFASM(MarioSetToSmallSingleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db $F8
%VWFASM(BowserSetToWavingArms)
%VWFASM(BowserJump)
db "Jeez, Mario, do you ", !CCSetFont, $02, "ever", !CCSetFont, $00, " stop blabbing? I don't care if some guy wants to borrow my minions to build some stupid factories! "
db "As long as he doesn't try to kidnap MY princess, he can do whatever he wants.", $EF, $EB

%VWFASM(MarioSetToSmallDoubleJumping)
%VWFASM(MarioSetWaitStateToNothing)
db $FF
db $FF
db "No, Junior has nothing to do with it this time! "
%VWFASM(BowserSetToWideEyes)
%VWFASM(BowserSetToRepeatedJumping)
db "What don't you get? I'm actually on vacation here, and ", !CCSetFont, $02, "you're", !CCSetFont, $00, " ruining it! "
db "Now get out of here! Just cause I'm relaxing doesn't mean I don't have the energy to ", !CCSetFont, $02, "pummel you flat like a panca--", $EB,  !CCSetFont, $00
%VWFASM(SetSpeakerToKamek)
db "Sire!", $EF
%VWFASM(BowserSetToNormalEyes)
%VWFASM(BowserSetToNormalArms)
%VWFASM(BowserStopJumping)
db $EB
db $F3
%VWFASM(KamekRunToBowser)
db "Please don't forget, sire! The doctor said that you need to keep your blood pressure down!", $EF, $EB
%VWFASM(SetSpeakerToBowser)
db "_", $FF, "Whatever.", $F6, $EB
db "Mario! You get it, right! Rest assured you'll get your pummeling another day, but today that's not my job! ", $EF, $EB
db "Now scram! "
%VWFASM(BowserJump)
%VWFASM(BowserSetToRelaxOnLanding)
db $FF
db $F9
db "You're blocking my sunlight.", $EF, $EB
db $E0

