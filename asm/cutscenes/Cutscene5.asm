!TanukiCHR1 = $023F
!TanukiCHR2 = $023A

!Cutscene5BackgroundCHR1 = $0126
!Cutscene5BackgroundCHR2 = $0124
!Cutscene5ForegroundTilemap = $0A2F
!Cutscene5BackgroundTilemap = $0A30

Cutscene5Palette:
incbin Cutscene5.rawpal
	
CSINIT5:
	STZ $2121
	%StandardDMA(#$2122, Cutscene5Palette, #512, #$00) 
	
	REP #$20
	LDA #!layer1MapAddr
	STA $2116
	SEP #$20
	
	REP #$30
	LDA #!TanukiCHR1			; \
	LDX #$C000>>1				; | Upload SP1
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!TanukiCHR2			; \
	LDX #$D000>>1				; | Upload SP2
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene5BackgroundCHR1		; \
	LDX #!layer1ChrAddr+$2000>>1		; | Upload background GFX 3
	LDY #!CS1GardenChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene5BackgroundCHR2		; \
	LDX #!layer1ChrAddr+$3000>>1		; | Upload background GFX 4
	LDY #!CS1BackgroundChrExGFXSize		; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene5ForegroundTilemap	; \
	LDX #!layer1MapAddr>>1			; | Upload the foreground tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	LDA #!Cutscene5BackgroundTilemap	; \
	LDX #!layer2MapAddr>>1			; | Upload the background tilemap
	LDY #!CS1SeaBackTilemapExGFXSize	; |
	JSL DecompressAndUploadGFXFile		; /
	
	;LDA #$0058
	;STA $1C
	;LDA #$FFE0
	;STA $1A
	
	
	SEP #$30
	
	LDA #$00			; \
	STA !backgroundColor		; |
	LDA #$00			; |
	STA !backgroundColor+1		; |
	LDA #$0B			; |
	STA !backgroundColor+2		; /
	
	REP #$20
	LDA #$0000				; \ Layer 1 X
	STA $1A					; /
	LDA #$0070				; \ Layer 1 Y
	STA $1C					; /
	LDA #$0000				; \ Layer 2 X
	STA $1E					; /
	LDA #$0068				; \ Layer 2 Y
	STA $20					; /
	SEP #$20
	
	;LDA #$0058
	;STA $1C
	;LDA #$FFE0
	;STA $1A
	
	
	SEP #$30
	
	LDA #$00
	STA !currentFont
	
	JSL TanukiCreate
	JSL MarioCreate
	LDA #$00 : JSL MarioSetXPos
	LDA #$55 : JSL MarioSetYPos
	LDA #$78 : JSL MarioSetTargetX
	;JSL MarioSetWaitStateToNothing 
	;JSL MarioSetToVarWalkRight
	
	LDA !tanukiIndex
	STA !currentSpeaker
	
	RTL


CS5T1:
db $FF
%VWFASM(MarioSetToVarWalkRight)

db $FF
db $FF
%VWFASM(SetSpeakerToTanuki)
db !CCSetFont, $00
db "The New Temple of Water--", $EE, "nice place, no? "
db "The old one_", $EE, "well, it wasn't ", !CCSetFont, $02, "quite", !CCSetFont, $00, " as adaptable as the element it was built in honor of. "
db "And so_", $EE, $F6
%VWFASM(TanukiSetToJump)
%VWFASM(TanukiSetToZoomToLeft)
db "Sploosh!", $EF, $EB

db "Ah, but I love the water, don't you? Ever flowing, ever changing; re-", $EE, "forming itself and carving out the world around it. "
db "A force for progress. Just like Mr. Norveg, no?"
%VWFASM(TanukiSetToZoomToRight)
db $EF, $EB

db "Ah, but I heard that Doc discovered a place where ", !CCSetFont, $02, "everything", !CCSetFont, $00, " flows and changes like the water! "
db "I'd like to go there someday. "
db "Oh, but I can't now--", $EE, "too much work to be done here. "
db "We're far behind schedule shaking up this sleepy little land, dragging it out of its stagnant little tidal pool and into the wave of the future."
%VWFASM(TanukiSetToBall)
%VWFASM(TanukiSetToZoomToLeft)
db $EF, $EB

db "Oh, but you know what they say--", $FF, !CCNewLine
%VWFASM(TanukiSetToJump)
%VWFASM(TanukiSetToZoomToRight)
db "Out with the old, ", $FF, !CCNewLine
%VWFASM(TanukiSetToBall)
%VWFASM(TanukiSetToZoomToLeft)
db "in with the new. ", $FF, !CCNewLine
%VWFASM(TanukiSetToJump)
%VWFASM(TanukiSetToZoomToRight)
db "Out with the old_", $FF, !CCNewLine
db "And so ", $FF, "(heh), ", $FE
%VWFASM(TanukiSetToZoomToLeft)
db "out with YOU.", $F9
%VWFASM(MarioAddExclamationMark)
db $FF, $FF, $EF, $FF, $E0
