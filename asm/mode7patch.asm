; This patch makes it so that SMW's Mode 7 mirrors as well as rotation and scaling routines can be used in any level where the BG Mode ($3E) is set to 7. It also disables ExAnimation in those levels.

header
lorom

; N/A !freespace = $39EBD6

;-------------------------------------

org $8081C6
autoclean JML MirrorCheck
	NOP

org $80A28A
autoclean JML ScrollRotateCheck
	NOP

org $80A5AF
autoclean JML ExAnimCheck
	NOP
	
org $0FC3DB
autoclean JML Mode7Pos


freecode ;org !freespace

;-------------------------------------


; This makes SMW use the Mode 7 mirrors in ANY Mode 7 level instead
; of just boss levels.

MirrorCheck:
	LDA $3E
	AND #$07
	CMP #$07
	BEQ .mode7level
	LDA $0D9B
	BMI .mode7level
	JML $8081CE

.mode7level
	;STA $0D9B
	JML $8082C4
	
Mode7Pos:
	LDA $3E
	AND #$07
	CMP #$07
	BEQ .mode7level
	LDA $0D9B
	BMI .mode7level
	LDA #$81
	STA $4200
	JML $80838F

.mode7level
	LDA #$81
	JML $8083BA

;-------------------------------------

; Turns off scrolling in Mode 7 levels (this function is disabled!)
; and handles rotation and scaling.

ScrollRotateCheck:
	LDA $3E
	AND #$07
	CMP #$07
	BEQ .mode7level
	LDA $0D9B
	BMI .bosslevel
	JML $80A295

.mode7level
	PHK
	PER $0006
	PEA $84CE	;PLB : RTL
	JML $80987D	;JSL to RTS
	JML $80A295

.bosslevel
	PHK
	PER $0006
	PEA $84CE	;PLB : RTL
	JML $80987D	;JSL to RTS
	JML $80A2A9

;-------------------------------------

; Turns off ExAnimation in Mode 7 levels.

ExAnimCheck:
	LDA $3E
	AND #$07
	CMP #$07
	BEQ .mode7level
	LDA $0D9B
	BMI .mode7level
	JML $80A5B9

.mode7level
	JML $80A5B4

;-------------------------------------
