lorom
header

org $01AF0A
	autoclean JML postering_prohibited

freecode

postering_prohibited:
	LDA $1588,x
	AND #$04
	BNE .on_ground
	LDA #$07		; change object clipping temporarily
	STA $1656,x
	JSL $819138
	LDA #$01
	STA $1656,x
	LDA $1588,x
	AND #$04
	BNE .on_ground
	JML $81AF23

.on_ground
	JML $81AF0F
