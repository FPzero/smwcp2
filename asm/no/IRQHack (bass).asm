//@bass-snes-cpu
arch snes.cpu
define header 0x200 //If no header, set to 0
macro seek n
  org (({n} & 0x7f0000) >> 1) | ({n} & 0x7fff) + {header}
  base {n}
endmacro

define freespace $3AC000	//Set freespace
define freeram $7E0087		//Set freeram, shared with the wheel
define LavaYPos $00140B

{seek $00FFD8}
db $05

{seek $0081AA }
jml IRQScanlineHack

{seek $0081AF }
IRQScanlineHackReturn:

{seek $008385 }
jml IRQCode

{seek $0083B2 }
IRQCodeReturn:

//Breaks the original bosses. We don't want the NMI to overwrite the stuff we set earlier
{seek $0082F7 }
nop; nop; nop; nop; nop

//Also breaks the original bosses, stops the lava from being done
{seek $009883 }
rts

{seek $008345 }
ldy {freeram}&$FFFF
jmp {+}

{seek $008294 }
+


{seek $008416 }
jml L1ScrollHijack

{seek $00841B }
L1ScrollReturnNoWheel:

{seek $008438 }
L1ScrollReturnWheel:

{seek {freespace} }
L1ScrollHijack:
	lda.l $7E1908
	cmp #$04
	bne .NotWheelBoss
		lda.l $00003A
		sta.l $00210D
		lda.l $00003B
		sta.l $00210D
		lda.l $00003C
		sta.l $00210E
		lda.l $00003D
		sta.l $00210E
		jml L1ScrollReturnWheel
	.NotWheelBoss:
		lda.b #$59
		sta.l $002107
		jml L1ScrollReturnNoWheel


IRQScanlineHack:
lda #$80				//Force blank
sta.l $002100
lda.l $7E1908
cmp #$04
bne {+}
lda.l {freeram}
sta.l $004209
+
jml IRQScanlineHackReturn

IRQCode:
lda.l $7E1908
cmp #$04
bne NotIRQ
lda.l {freeram}
beq NotIRQ
-
bit.w $4212
bvs {-}	
-
bit.w $4212
bvc {-}//Wait for hblank
lda #$01
sta.l $002105
lda.b #(0x5400>>10<<2|1)
sta $002107
lda.b #((0xA000>>13)|(0xA000>>9))
sta $00210B
//Layer 1 positioning
rep #$20
lda $00001c
sec
sbc #$0180
sep #$20
sta $00210E
xba
sta $00210E

;lda #$11
;sta $212C
;stz $212D


lda $00001A
sta.l $00210D
lda.l $00001B
sta.l $00210D



NotIRQ:
jml IRQCodeReturn