lorom
header

; sublevel to do shenaningans in
!Level = $0100

org $0081F0
	autoclean JML SomeLabel
	
freecode

SomeLabel:
	BNE Return1			;\ restore hijacked code
	BCS Return2			;/
	REP #$20			;\ do stuff only if sdgfdg
	LDA $010B			; |
	CMP #!Level			; |
	SEP #$20			; |
	BNE Return3			;/
	JSR ASDFlabel
Return2:
	JML $8081F7
Return3:
	JML $8081F4
Return1:
	JML $808222

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Main code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
ASDFlabel:
	STZ $2115			; increment on $2118 writes
	REP #$20
	LDA #$5060			; VRAM destination
	STA $2116
	LDA #$1800			; CPU->PPU also write to $2118
	STA $4310
	LDA #$A020			; source address
	STA $4312
	LDX #$7F			; source bank
	STX $4314
	LDA #$02E0			; transfer size
	STA $4315
	LDX #$02			; update tiles with DMA
	STX $420B
	
	LDX #$80			; increment on $2119 writes
	STX $2115
	LDA #$51E0			; VRAM destination (was $5160)
	STA $2116
	LDX #$19			; write to $2119 this time
	STX $4311
	LDA #$A300			; just set source address, bank is still $7F
	STA $4312
	LDA #$00A0			; transfer size
	STA $4315
	LDX #$02
	STX $420B			; update first properties area with DMA
	
	LDA #$52E0			; VRAM destination (was $52A0)
	STA $2116
	LDA #$A3A0			; source address
	STA $4312
	LDX #$60			; transfer size etc
	STX $4315
	LDX #$02
	STX $420B			; update second properties area with DMA
	
	SEP #$20
	RTS
