table table.txt

; author IDs
!AXEMJINX	= $00*2
!BLOOP		= $01*2
!BUSTER_BEETLE	= $02*2
!CK_CRASH	= $03*2
!CRISPYYOSHI	= $04*2
!KIPERNAL	= $05*2
!JIMMY52905	= $06*2
!KIL		= $07*2
!LUNAR_WURMPLE	= $08*2
!MASASHI27	= $09*2
!MIDIGUY	= $0A*2
!MOOSE		= $0B*2
!MVS		= $0C*2
!QBEE		= $0D*2
!QBEE_AND_BLOOP	= $0E*2
!RED_CHAMELEON	= $0F*2
!REDNGREEN	= $10*2
!SNN		= $11*2
!SLASH_MAN	= $12*2
!TODD		= $13*2
!TORNADO	= $14*2
!TRAPFANATIC	= $15*2
!YONOWAARU	= $16*2
!TORCHKAS	= $17*2
!KOJI_KONDO	= $18*2

; format: db size,"NAME"
; each name must be no less than 24 characters long
song00:	db $0B,"SIMPLE SLIP"
song01:	db $0E,"TREETOP TUMBLE"
song02:	db $0F,"NANAKOROBIYAOKI"
song03:	db $13,"DROWNING DEHYDRATED"
song04:	db $0F,"SHOW'S CANCELED"
song05:	db $11,"FALLING FOR HOURS"
song06:	db $13,"PARADOX PANDEMONIUM"
song07:	db $16,"NO O.S.H.A. COMPLIANCE"
song08:	db $15,"PHANTASMAGORICAL FLUB"
song09:	db $09,"GAME OVER"
song10:	db $0A,"BOSS CLEAR"
song11:	db $0B,"STAGE CLEAR"
song12:	db $0A,"STAR BOMBA"
song13:	db $0F,"SWITCH SCRAMBLE"
song14:	db $07,"KEYHOLE"
song15:	db $08,"IRIS OUT"
song16:	db $0C,"INTRO SCREEN"
song17:	db $11,"FANTASTIC FANFARE"
song18:	db $13,"INNOCENT BEGINNINGS"
song19:	db $10,"MECHANICAL MOTIF"
song20:	db $12,"PUMPED-UP PRESSURE"
song21:	db $08,"ATHLETIC"
song22:	db $0E,"PLAYFUL BREEZE"
song23:	db $12,"SOMBER STALAGMITES"
song24:	db $12,"BRACE FOR A BONUS!"
song25:	db $11,"PALACES OF UNEASE"
song26:	db $15,"INDUSTRIAL REVOLUTION"
song27:	db $0B,"BE THE HERO"
song28:	db $16,"TREMBLE AMID THE TREES"
song29:	db $16,"PAPERFOLIAGE ORCHESTRA"
song30:	db $11,"ALL THE LEAVES..."
song31:	db $12,"SUSPICIOUS SILENCE"
song32:	db $0D,"MOONLIT RIVER"
song33:	db $16,"MELANCHOLIC FOREST AIR"
song34:	db $13,"FLORIST GHOST HOUSE"
song35:	db $0B,"CRYSTALLINE"
song36:	db $11,"FLUTES OF DESPAIR"
song37:	db $06,"SAKURA"
song38:	db $06,"MIZUMI"
song39:	db $10,"DOUKUTSU NO KAZE"
song40:	db $14,"IN THE EASTERN WOODS"
song41:	db $0D,"ORIENTAL OPUS"
song42:	db $0D,"BAMBOO BUSTLE"
song43:	db $10,"SONG OF THE SEAS"
song44:	db $0D,"SPRING BREEZE"
song45:	db $0D,"WATERY DEPTHS"
song46:	db $09,"WATERWAYS"
song47:	db $0B,"SEA OF MIST"
song48:	db $0D,"MERCHANT SONG"
song49:	db $08,"ENTRANCE"
song50:	db $11,"PARCHED POLYPHONY"
song51:	db $0D,"SANDY STRINGS"
song52:	db $10,"CULT OF CALAMITY"
song53:	db $09,"RUSH TIME"
song54:	db $0C,"TWISTED TUNE"
song55:	db $05,"SHACO"
song56:	db $13,"FUN AT THE CARNIVAL"
song57:	db $0E,"ROLLER COASTER"
song58:	db $13,"LET THE SHOW BEGIN!"
song59:	db $08,"FLOATING"
song60:	db $0A,"TO THE SKY"
song61:	db $03,"LUX"
song62:	db $10,"FLOATING CITADEL"
song63:	db $10,"HEAVIER THAN AIR"
song64:	db $10,"CAVERNOUS CHORDS"
song65:	db $13,"UNDESERVED REGALITY"
song66:	db $15,"CALM BEFORE THE STORM"
song67:	db $0F,"BURNT ICE BEATS"
song68:	db $0E,"ASHEN SNOWFALL"
song69:	db $08,"OVERBURN"
song70:	db $17,"MAGNIFICENT MAGMA MURAL"
song71:	db $11,"BELLS OF PARADICE"
song72:	db $12,"WHITE WINTER NIGHT"
song73:	db $0C,"MARIO ON ICE"
song74:	db $15,"CHECK THE TEMPERATURE"
song75:	db $13,"NORVEG INCORPORATED"
song76:	db $16,"IRON NOTES OF INDUSTRY"
song77:	db $17,"MACHINE-MADE MELANCHOLY"
song78:	db $0D,"FACTORY FUGUE"
song79:	db $0D,"ONE LAST HOPE"
song80:	db $0F,"AUTOMATED ANGER"
song81:	db $0A,"DREAMSCAPE"
song82:	db $0C,"ELECTROSPIRE"
song83:	db $10,"SIX-EIGHT SORROW"
song84:	db $05,"LUCID"
song85:	db $11,"BELLS OF ETERNITY"
song86:	db $12,"SORROWFUL BALLOONS"

AuthorIDs:
	db !KIPERNAL		; song 00
	db !KIPERNAL		; song 01
	db !KIPERNAL		; song 02
	db !KIPERNAL		; song 03
	db !KIPERNAL		; song 04
	db !KIPERNAL		; song 05
	db !KIPERNAL		; song 06
	db !KIPERNAL		; song 07
	db !KIPERNAL		; song 08
	db !KOJI_KONDO		; song 09
	db !MOOSE		; song 10
	db !MOOSE		; song 11
	db !JIMMY52905		; song 12
	db !TODD		; song 13
	db !KOJI_KONDO		; song 14
	db !KOJI_KONDO		; song 15
	db !TORCHKAS		; song 16
	db !TODD		; song 17
	db !KIPERNAL		; song 18
	db !TODD		; song 19
	db !BUSTER_BEETLE	; song 20
	db !JIMMY52905		; song 21
	db !CK_CRASH		; song 22
	db !KIPERNAL		; song 23
	db !TODD		; song 24
	db !KIPERNAL		; song 25
	db !SNN		        ; song 26
	db !MOOSE		; song 27
	db !RED_CHAMELEON	; song 28
	db !SLASH_MAN		; song 29
	db !BUSTER_BEETLE	; song 30
	db !KIPERNAL		; song 31
	db !SLASH_MAN		; song 32
	db !LUNAR_WURMPLE	; song 33
	db !RED_CHAMELEON	; song 34
	db !SNN		        ; song 35
	db !JIMMY52905		; song 36
	db !SNN		        ; song 37
	db !JIMMY52905		; song 38
	db !YONOWAARU		; song 39
	db !BUSTER_BEETLE	; song 40
	db !TODD		; song 41
	db !AXEMJINX		; song 42
	db !MVS		        ; song 43
	db !JIMMY52905		; song 44
	db !REDNGREEN		; song 45
	db !SNN		        ; song 46
	db !JIMMY52905		; song 47
	db !MOOSE		; song 48
	db !MOOSE		; song 49
	db !TODD		; song 50
	db !BLOOP		; song 51
	db !CK_CRASH		; song 52
	db !REDNGREEN		; song 53
	db !TODD		; song 54
	db !KIL		        ; song 55
	db !MIDIGUY		; song 56
	db !MOOSE		; song 57
	db !QBEE_AND_BLOOP	; song 58
	db !JIMMY52905		; song 59
	db !MOOSE		; song 60
	db !KIL		        ; song 61
	db !SNN		        ; song 62
	db !TORNADO		; song 63
	db !TODD		; song 64
	db !KIPERNAL		; song 65
	db !SNN		        ; song 66
	db !TODD		; song 67
	db !JIMMY52905		; song 68
	db !SNN		        ; song 69
	db !KIPERNAL		; song 70
	db !JIMMY52905		; song 71
	db !BUSTER_BEETLE	; song 72
	db !JIMMY52905		; song 73
	db !QBEE		; song 74
	db !SNN		        ; song 75
	db !SLASH_MAN		; song 76
	db !MASASHI27		; song 77
	db !SLASH_MAN		; song 78
	db !REDNGREEN		; song 79
	db !REDNGREEN		; song 80
	db !SNN		        ; song 81
	db !CRISPYYOSHI		; song 82
	db !TRAPFANATIC		; song 83
	db !JIMMY52905		; song 84
	db !MOOSE		; song 85
	db !SNN		        ; song 86

; size,"NAME" blah
; 21 characters blah
author00:
	db $08,"AXEMJINX"
author01:
	db $05,"BLOOP"
author02:
	db $0D,"BUSTER BEETLE"
author03:
	db $08,"CK CRASH"
author04:
	db $0B,"CRISPYYOSHI"
author05:
	db $08,"KIPERNAL"
author06:
	db $0A,"JIMMY52905"
author07:
	db $03,"KIL"
author08:
	db $0D,"LUNAR WURMPLE"
author09:
	db $09,"MASASHI27"
author0A:
	db $07,"MIDIGUY"
author0B:
	db $05,"MOOSE"
author0C:
	db $03,"MVS"
author0D:
	db $05,"Q-BEE"
author0E:
	db $0F,"Q-BEE AND BLOOP"
author0F:
	db $0D,"RED CHAMELEON"
author10:
	db $09,"REDNGREEN"
author11:
	db $06,"S.N.N."
author12:
	db $09,"SLASH MAN"
author13:
	db $04,"TODD"
author14:
	db $07,"TORNADO"
author15:
	db $0B,"TRAPFANATIC"
author16:
	db $09,"YONOWAARU"
author17:
	db $08,"TORCHKAS"
author18:
	db $0A,"KOJI KONDO"

SongNamePtrs:
	dw song00
	dw song01
	dw song02
	dw song03
	dw song04
	dw song05
	dw song06
	dw song07
	dw song08
	dw song09
	dw song10
	dw song11
	dw song12
	dw song13
	dw song14
	dw song15
	dw song16
	dw song17
	dw song18
	dw song19
	dw song20
	dw song21
	dw song22
	dw song23
	dw song24
	dw song25
	dw song26
	dw song27
	dw song28
	dw song29
	dw song30
	dw song31
	dw song32
	dw song33
	dw song34
	dw song35
	dw song36
	dw song37
	dw song38
	dw song39
	dw song40
	dw song41
	dw song42
	dw song43
	dw song44
	dw song45
	dw song46
	dw song47
	dw song48
	dw song49
	dw song50
	dw song51
	dw song52
	dw song53
	dw song54
	dw song55
	dw song56
	dw song57
	dw song58
	dw song59
	dw song60
	dw song61
	dw song62
	dw song63
	dw song64
	dw song65
	dw song66
	dw song67
	dw song68
	dw song69
	dw song70
	dw song71
	dw song72
	dw song73
	dw song74
	dw song75
	dw song76
	dw song77
	dw song78
	dw song79
	dw song80
	dw song81
	dw song82
	dw song83
	dw song84
	dw song85
	dw song86

AuthorNamePtrs:
	dw author00
	dw author01
	dw author02
	dw author03
	dw author04
	dw author05
	dw author06
	dw author07
	dw author08
	dw author09
	dw author0A
	dw author0B
	dw author0C
	dw author0D
	dw author0E
	dw author0F
	dw author10
	dw author11
	dw author12
	dw author13
	dw author14
	dw author15
	dw author16
	dw author17
	dw author18
