;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Normal Shooter 2, based on the Bullet Bill Shooter disassembly by mikeyk, further 
;; adapted into Sprite Tool by Davros   
;;
;; Description: This will generate a normal sprite without smoke.
;; Specify the actual sprite and sound that is generated below.
;;
;; NOTE: Trying to generate a sprite that doesn't exist will crash your game.
;;
;; Uses first extra bit: NO
;;  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    !SPRITE_TO_GEN = $39
                    !SOUND_TO_GEN = $09

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
                    PRINT "INIT ",pc              
                    PRINT "MAIN ",pc                                    
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                     
                    RTL      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main bullet bill shooter code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;               

SPRITE_CODE_START:   LDA $17AB,x             ; \ RETURN if it's not time to generate
                    BNE RETURN              ; /
                    LDA #$60                ; \ set time till next generation = 60
                    STA $17AB,x             ; /
                    LDA $178B,x             ; \ don't generate if off screen vertically
                    CMP $1C                 ;  |
                    LDA $1793,x             ;  |
                    SBC $1D                 ;  |
                    BNE RETURN              ; /
                    LDA $179B,x             ; \ don't generate if off screen horizontally
                    CMP $1A                 ;  |
                    LDA $17A3,x             ;  |
                    SBC $1B                 ;  |
                    BNE RETURN              ; / 
                    LDA $179B,x             ; \ ?? something else related to x position of generator??
                    SEC                     ;  | 
                    SBC $1A                 ;  |
                    CLC                     ;  |
                    ADC #$10                ;  |
                    CMP #$10                ;  |
                    BCC RETURN              ; /
                    LDA $94                 ; \ don't fire if mario is next to generator
                    SBC $179B,x             ;  |
                    CLC                     ;  |
                    ADC #$11                ;  |
                    CMP #$22                ;  |
                    BCC RETURN              ; /
                    JSL $02A9DE             ; \ get an index to an unused sprite slot, RETURN if all slots full
                    BMI RETURN              ; / after: Y has index of sprite being generated

GENERATE_SPRITE:     LDA #!SOUND_TO_GEN       ; \ play sound effect
                    STA $1DFC               ; /
                    LDA #$01                ; \ set sprite status for new sprite
                    STA $14C8,y             ; /
                    LDA #!SPRITE_TO_GEN      ; \ set sprite number for new sprite
                    STA $009E,y             ; /
                    LDA $179B,x             ; \ set x position for new sprite
                    STA $00E4,y             ;  |
                    LDA $17A3,x             ;  |
                    STA $14E0,y             ; /
                    LDA $178B,x             ; \ set y position for new sprite
                    SEC                     ;  | (y position of generator - 1)
                    SBC #$01                ;  |
                    STA $00D8,y             ;  |
                    LDA $1793,x             ;  |
                    SBC #$00                ;  |
                    STA $14D4,y             ; /
                    PHX                     ; \ before: X must have index of sprite being generated
                    TYX                     ;  | routine clears *all* old sprite values...
                    JSL $07F7D2             ;  | ...and loads in new values for the 6 main sprite tables
                    PLX                     ; / 
RETURN:              RTS                     ; RETURN

