;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Bone Pile
; By Sonikku
; 
; Description: A SPRITE that stays wherever you put it, and if 
; Mario gets close, it turns into a Dry Bones that is getting up
; (Rather than being a completely new SPRITE altogether).
; It turns into a normal Dry Bones or a Dry Bones that throws
; bones depending on its X position.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!Time = $30		; time before getting up
!Range = $70

	PRINT "INIT ",pc
	LDA $E4,x
	LSR
	LSR
	LSR
	LSR
	AND #$01
	STA $C2,x
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main SPRITE JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	PRINT "MAIN ",pc
	PHB
	PHK
	PLB
	JSR SPRITEMAIN
	PLB
	RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; main SPRITE routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
SPRITE:	db $30,$32
SPRITEMAIN:
	JSR SUB_GFX		; load graphics
	JSR SUB_OFF_SCREEN_X0 ; only process if on screen
	LDA $9D		; if sprites locked..
	BNE RETURN	; RETURN
	LDA #$08	; if sprites status..
	CMP $14C8,x	; isn't 8..
	BNE RETURN	; RETURN
	JSL $01802A	; update position based on speed
	LDA $1588,x	; if SPRITE is in the air
	AND #$04	; 
	BEQ NOGND	; it has y speed
	STZ $AA,x	; if on ground, no y speed
NOGND:	JSR SUB_HORZ_POS ; determine..
	LDA $0F		; if mario..
	CLC		; is close..
	ADC #!Range	; to this..
	CMP #$50	; SPRITE..
	BCS RETURN	; if he isn't, RETURN
	LDY $C2,x	; index by x position..
	LDA SPRITE,y	; load SPRITE to transform into..
	STA $9E,x	; and store it.
	JSL $07F7D2	; reset init
	LDA #$08	; SPRITE's status..
	STA $14C8,x	; is 8.
	LDA #$01	; sprites stomp status..
	STA $1534,x	; is 1 now.
	LDA #!Time	; set timer before..
	STA $1540,x	; getting up.
	LDA #$07	; play sound effect.
	STA $1DF9	; and store it
	LDA #$01	; make it face the same..
	STA $157C,x	; direction as original SPRITE.
RETURN:	RTS		; RETURN.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:		db $2D,$2E
XDISP:		db $F8,$00
SUB_GFX:
	JSL $20CA4F

	LDA $15F6,x
	STA $02

	LDX #$00

LOOP:	LDA $00
	CLC
	ADC XDISP,x
	STA $0300,y

	LDA $01
	STA $0301,y

	LDA TILEMAP,x
	STA $0302,y

	LDA $02
	ORA $64
	STA $0303,y

	INY
	INY
	INY
	INY

	INX
	CPX #$02
	BNE LOOP

	LDX $15E9
	LDY #$02
	LDA #$01
	JSL $01B7B3
	RTS



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; off screen processing code - shared
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	;org $03B83B             

TABLE3:              db $40,$B0
TABLE6:              db $01,$FF 
TABLE4:              db $30,$C0,$A0,$80,$A0,$40,$60,$B0 
TABLE5:              db $01,$FF,$01,$FF,$01,$00,$01,$FF

SUB_OFF_SCREEN_X0:   LDA #$06                ; \ entry point of routine determines value of $03
	BRA STORE_03            ; | 
SUB_OFF_SCREEN_X1:   LDA #$04                ; |
	BRA STORE_03            ; |
SUB_OFF_SCREEN_X2:   LDA #$02                ; |
STORE_03:            STA $03                 ; |
	BRA START_SUB           ; |
SUB_OFF_SCREEN_X3:  STZ $03                 ; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if SPRITE is not off screen, RETURN
	BEQ RETURN_2            ; /    
	LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
	AND #$01                ; |
	BNE VERTICAL_LEVEL      ; /     
	LDA $D8,x               ; \
	CLC	 ; | 
	ADC #$50                ; | if the SPRITE has gone off the bottom of the level...
	LDA $14D4,x             ; | (if adding 0x50 to the SPRITE y position would make the high byte >= 2)
	ADC #$00                ; | 
	CMP #$02                ; | 
	BPL ERASE_SPRITE        ; /    ...erase the SPRITE
	LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
	AND #$04                ; |
	BNE RETURN_2            ; /
	LDA $13                 ; \ 
	AND #$01                ; | 
	ORA $03                 ; | 
	STA $01                 ; |
	TAY	 ; /
	LDA $1A
	CLC
	ADC TABLE4,y
	ROL $00
	CMP $E4,x
	PHP
	LDA $1B
	LSR $00
	ADC TABLE5,y
	PLP
	SBC $14E0,x
	STA $00
	LSR $01
	BCC LABEL20
	EOR #$80
	STA $00
LABEL20:             LDA $00
	BPL RETURN_2
ERASE_SPRITE:        LDA $14C8,x             ; \ if SPRITE status < 8, permanently erase SPRITE
	CMP #$08                ; |
	BCC KILL_SPRITE         ; /
	LDY $161A,x
	CPY #$FF
	BEQ KILL_SPRITE
	LDA #$00
	STA $1938,y
KILL_SPRITE:         STZ $14C8,x             ; erase SPRITE
RETURN_2:            RTS	 ; RETURN

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, RETURN
	AND #$04                ; |
	BNE RETURN_2            ; /
	LDA $13                 ; \ only handle every other frame??
	LSR A                   ; | 
	BCS RETURN_2            ; /
	AND #$01
	STA $01
	TAY
	LDA $1C
	CLC
	ADC TABLE3,y
	ROL $00
	CMP $D8,x
	PHP
	LDA.w $001D
	LSR $00
	ADC TABLE6,y
	PLP
	SBC $14D4,x
	STA $00
	LDY $01
	BEQ LABEL22
	EOR #$80
	STA $00
LABEL22:             LDA $00
	BPL RETURN_2
	BMI ERASE_SPRITE

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if SPRITE is on screen, accumulator = 0 
	ORA $186C,x             ; |  
	RTS	 ; / RETURN

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/SPRITE check - shared
; Y = 1 if mario left of SPRITE??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B817                ; Y = 1 if contact

SUB_HORZ_POS:         LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0F                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642
		