	PRINT "INIT ",pc
		RTL

	PRINT "MAIN ",pc
		PHB		;\
		PHK		; | Change the data bank to the one our code is running from.
		PLB		; | This is a good practice.
		JSR SPRITECODE	; | Jump to the sprite's function.
		PLB		; | Restore old data bank.
		RTL		;/ And RETURN.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Sprite MAIN Code			;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SPRITECODE:
		LDA $77		;\ Check if mario is blocked from below
		CMP #$04	;| If he isn't blocked from below, RETURN
		BNE RETURN	;/
		LDA $18		;\
		AND #$80	;| If pressing A, branch
		BNE SWITCH	;/
		RTS		; Else, RETURN

SWITCH:
		LDA #$0B	;\ Make the Hit
		STA $1DF9	;/ SWITCH Sound
		LDA $14AF	;\
		EOR #$01	;| SWITCH between On & Off
		STA $14AF	;/
RETURN:				;\
		RTS		;/ RETURN