incsrc "subroutinedefs_xkas.asm"

; original sprite by ixtab

; DOESN'T WORK YET

; !ram = $18A6
; !sprnumber = $B9 ;insert number

; const
!max_hp			= 5

!eyelid_props		= $23
!pupil_props		= $03

!hurt_sfxn		= $28
!hurt_sfxb		= $1DFC

; ram
!x_pos_lo		= $E4
!x_pos_hi		= $14E0
!y_pos_lo		= $D8
!y_pos_hi		= $14D4
!x_speed		= $B6
!y_speed		= $AA
!spr_status		= $14C8

; ram misc
!curr_act		= $C2
!next_act		= $160E
!act_timer		= $163E
!act_count		= $1626		; for AI purposes

!health			= $1504

!eye_flag		= $1510		; 0 = closing?
!eyelid_frame		= $1602		; to keep track of the closing process

!bomb_drop_timer	= $1540		; maybe "max" frames to wait for next drop

; !tweaker_5 = $1686
; !tweaker_3 = $166E
; !tweaker_4 = $167A

; automagically assings IDs and shit
!a = 0
macro act(name)
	dw <name>
	!act_<name> = !a
	!a #= !a+1
endmacro
	
action_ptrs:
	%act(wait)
	%act(what_do)
	%act(idle)
	%act(drop_bomb)
	%act(wires)
	%act(roots)
	%act(hurt)
	%act(finished)

; 2nd phase acts?
	; %act(idle_2)
	; %act(move_right)
	; %act(move_left)
	; %act(move_around_right)
	; %act(move_around_left)
	
; actions PLUS, only for the very best TM
	; %act(wires_blind)
	; %act(wires_sideways)

	; %act(roots_blind)
	; %act(wires_hell)

print "INIT ",pc
	LDA #!max_hp
	STA !health,x
	
	STZ !curr_act,x
	LDA #$10
	STA !act_timer,x
	LDA #!act_what_do
	STA !next_act,x
	
	LDA #$01			; open eye
	STA !eye_flag,x

	; LDA #$70			; dik
	; STA !x_pos_lo,x
	; STZ !x_pos_hi,x
	; LDA #$20
	; STA !y_pos_lo,x
	; LDA #$01
	; STA !y_pos_hi,x
	
	RTL

	
print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR main
	PLB
	RTL
	
main:
	JSR draw_gfx
	LDA !spr_status,x
	EOR #$08
	ORA $9D
	BNE wait_return
	
.run
	LDA !curr_act,x
	ASL
	TAY
	REP #$20
	LDA action_ptrs,y
	STA $00
	SEP #$20
	JMP ($0000)
	
; $01801A: The subroutine that updates a sprite's Y position without gravity.
; $018022: The subroutine that updates a sprite's X position without gravity.

; wait until the next action (doing nothing)
wait:
	JSR interact_spr
	
	LDA !act_timer,x
	BNE .return
	LDA !next_act,x
	STA !curr_act,x
.return
	RTS

; kind of like wait but the eye may be closed etc (depending on hp)
idle:
	RTS

!acts_per_hp = 5
; the smartest action!!
what_do:
	LDA #!max_hp
	SEC
	SBC !health,x			; (hp*5 + acts)*2
	ASL
	ASL
	ADC !health,x
	ADC !act_count,x
	ASL
	TAY
	REP #$20
	LDA .acts,y
	STA $00
	SEP #$20
	PEA .post_act
	JMP ($0000)
	
.post_act
	INC !health,x
	LDA #!max_hp
	SEC
	SBC !health,x
	TAY
	LDA !act_count,x
	CMP .reset_counts,y
	RTS
	
	
.reset_counts
	db $03, $01, $01, $01, $01, $01, $01
	
.acts
	dw .wire_1, .idle_1, .bomb_1, 0, 0
	dw 0, 0, 0, 0, 0
	dw 0, 0, 0, 0, 0
	dw 0, 0, 0, 0, 0
	dw 0, 0, 0, 0, 0
	dw 0, 0, 0, 0, 0
	
	

; maybe I can just use tables
	; LDA !health,x
	; ASL
	; TAX
	; REP #$20
	; LDA .ptrs,x
	; STA $00
	; SEP #$20
	; LDX $15E9
	; JMP ($0000)
	
; .ptrs
	; dw what_do_0hp ; isn't this dead
	; dw what_do_1hp
	; dw what_do_2hp
	; dw what_do_3hp
	; dw what_do_4hp
	; dw what_do_5hp
	

; what_do_5hp:
	
	
	RTS

drop_bomb:
wires:
roots:


hurt:


finished:
	RTS
	
; ----------------------------------------------------
interact_spr:
	LDA !eye_flag
	BEQ .return
	JSL $83B6E5			; this sprite is clipping b
	PHX
	LDX #$0B
.spr_loop
	LDA !spr_status,x
	CMP #$09
	BCC .next
	CPX $15E9
	BEQ .next
	JSL $83B69F			; other sprite is clipping a
	JSL $83B72B			; collision check
	BCS .hit
.next
	DEX
	BPL .spr_loop
	PLX
.return
	RTS
	
.hit
	STZ !spr_status,x		; kill bomb
	DEC !health,x
	PLX
	LDA #!hurt_sfxn
	STA !hurt_sfxb
	LDA #$30			; shake ground
	STA $1887
	LDA #$40			; enter hurt state
	STA !act_timer,x
	LDA #!act_hurt
	STA !curr_act,x
	PLA				; override current action
	PLA
	RTS

; ----------------------------------------------------
draw_gfx:
	JSL !GetDrawInfo
	
	LDA !eyelid_frame,x		; x16
	LSR
	LSR
	; LSR
	; LSR
	; ASL
	; ASL
	STA $02
	PHX
	LDX #$03
.eyelid_loop
	LDA $00				; x pos
	CLC
	ADC .eyelid_x_disp,x
	STA $0300,y
	LDA $01				; y pos
	CLC
	ADC .eyelid_y_disp,x
	STA $0301,y
	PHX				; tile
	TXA
	CLC
	ADC $02
	TAX
	LDA .eyelid_tilemap,x
	STA $0302,y
	PLX
	LDA #!eyelid_props		; props
	ORA $64
	STA $0303,y
	INY
	INY
	INY
	INY
	DEX
	BPL .eyelid_loop
	PLX
	
	LDA !eye_flag,x
	BEQ .closing
	LDA !eyelid_frame,x
	CMP #$20
	BEQ +
	INC !eyelid_frame,x
	BRA +
	
.closing
	LDA !eyelid_frame,x
	BEQ +
	DEC !eyelid_frame,x
+
	
	LDA !curr_act,x
	CMP #!act_hurt
	BEQ .roll_eye
	
	PHX
	REP #$20		; x=(-sx+128)+px
	LDA $00
	AND #$00FF
	EOR #$FFFF
	INC
	CLC
	ADC #$0080
	CLC
	ADC $7E
	BPL +
	LDA #$0000
	BRA ++
+	CMP #$0100
	BMI ++
	LDA #$00F0
++
	SEP #$20
	LSR
	LSR
	LSR
	LSR
	TAX
	LDA $00
	CLC
	ADC .pupil_x_disp,x
	STA $0300,y
	
	REP #$20		; same thing I guess?
	LDA $01
	AND #$00FF
	EOR #$FFFF
	INC
	CLC
	ADC #$0080
	CLC
	ADC $80
	BPL +
	LDA #$0000
	BRA ++
+	CMP #$0100
	BMI ++
	LDA #$00F0
++
	SEP #$20
	LSR
	LSR
	LSR
	LSR
	TAX
	LDA $01
	CLC
	ADC .pupil_y_disp,x
	STA $0301,y
	PLX

.finish
	LDA #$2A
	STA $0302,y
	LDA #!pupil_props
	ORA $64
	STA $0303,y
	
	LDY #$02			; 16x16
	LDA #$04			; 3 tiles
	JSL $81B7B3
	RTS
	
.roll_eye
	LDA $14
	LSR
	LSR
	AND #$07
	PHX
	TAX
	LDA $00
	CLC
	ADC .rolling_x_disp,x
	STA $0300,y
	LDA $01
	CLC
	ADC .rolling_y_disp,x
	STA $0301,y
	PLX
	BRA .finish


.eyelid_x_disp
	db $00,$10,$00,$10

.eyelid_y_disp
	db $00,$00,$10,$10
	
.eyelid_tilemap
	db $28,$28,$28,$28
	db $40,$42,$44,$46
	db $60,$62,$64,$66

.pupil_x_disp
	db $04,$04,$05,$06
	db $08,$08,$09,$0B
	db $0C,$0E,$0F,$10
	db $12,$13,$14,$14

.pupil_ydisp
	db $04,$04,$04,$04
	db $04,$04,$04,$04
	db $08,$0A,$0C,$0E
	db $10,$10,$10,$10
	
	
