incsrc subroutinedefs_xkas.asm


SineTable:	db $00,$00,$01,$01,$02,$02,$02,$03,$03,$04,$04,$04,$05,$05,$05,$06,$06,$06,$06,$07,$07,$07,$07,$07,$08,$08,$08,$08,$08,$08
		db $08,$08,$08,$08,$08,$08,$08,$07,$07,$07,$07,$07,$06,$06,$06,$06,$05,$05,$05,$04,$04,$04,$03,$03,$02,$02,$02,$01,$01,$00
		db $00,$00,$FF,$FF,$FE,$FE,$FE,$FD,$FD,$FC,$FC,$FC,$FB,$FB,$FB,$FA,$FA,$FA,$FA,$F9,$F9,$F9,$F9,$F9,$F8,$F8,$F8,$F8,$F8,$F8
		db $F8,$F8,$F8,$F8,$F8,$F8,$F8,$F9,$F9,$F9,$F9,$F9,$FA,$FA,$FA,$FA,$FB,$FB,$FB,$FC,$FC,$FC,$FD,$FD,$FE,$FE,$FE,$FF,$FF,$00

Tiles:		db $AC,$AC		;Cape
		db $A8,$A8		;Feet
		db $8A,$8A,$AA,$AA	;Body
		db $8C,$8D		;Hat
					;Note: hand/mouth are handled in the graphics routine

XOffsets:	db $00,$10
		db $00,$10
		db $00,$10,$00,$10
		db $FF,$07

YOffsets:	db $18,$18
		db $1F,$1F
		db $00,$00,$10,$10
		db $F8,$F8

Properties:	db %00110111,%01110111
		db %00110111,%01110111
		db %00110011,%01110011,%00110011,%01110011
		db %00110111,%00110111

TimerTiles:	db $C0,$C2,$C4,$C6,$C8,$CA,$CC,$CE,$E0,$E2

HandX:		db $1F,$1F,$1F,$1E
		db $1E,$1D,$1C,$1B
		db $1A,$19,$18,$17
		db $16,$15,$14,$13
		db $12,$11,$10,$0F

		db $0F,$10,$11,$12
		db $13,$14,$15,$16
		db $17,$18,$19,$1A
		db $1B,$1C,$1D,$1E
		db $1E,$1F,$1F,$1F

HandY:		db $19,$18,$17,$16
		db $15,$14,$13,$12
		db $12,$11,$11,$11
		db $10,$10,$10,$10
		db $10,$10,$10,$10

		db $10,$10,$10,$10
		db $10,$10,$10,$10
		db $0F,$0F,$0F,$0E
		db $0E,$0D,$0C,$0B
		db $0A,$09,$08,$07


;HandX:		db 31,31,31,31		;Alternate hand movement that is more mathematically correct
;		db 30,29,29,28		;but ended up looking worse.
;		db 27,27,26,25
;		db 24,22,21,20
;		db 19,18,16,15
;
;		db 15,16,18,19
;		db 20,21,22,24
;		db 25,26,27,27
;		db 28,29,29,31
;		db 30,31,31,31
;
;HandY:		db 25,25,25,25
;		db 25,24,24,24
;		db 23,23,22,22
;		db 21,21,20,19
;		db 19,18,17,16
;
;		db 16,15,14,13
;		db 13,12,12,11
;		db 10,10,09,09
;		db 08,08,08,07
;		db 07,07,07,07

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
STZ $14
STZ $1594,x
LDA $D8,x
LSR #4
STA $151C,x
LDA $E4,x
LSR #4
STA $1534,x
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR Main
PLB
RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MarioHasWon2:
JMP MarioHasWon
Quit:
JSR Graphics
RTS

Main:
LDA $9D
BNE Quit
REP #$20
LDA $1A
CLC
ADC #$00B8
STA $00
LDA $1C
CLC
ADC #$0018
STA $02
SEP #$20

LDA $00
STA $E4,x
LDA $01
STA $14E0,x
LDA $02
STA $D8,x
LDA $03
STA $14D4,x

LDA $160E,x
BNE MarioHasWon

LDA $71
CMP #$09
BEQ SkipToGraphics


INC $1594,x
LDA $1594,x
CMP #119
BNE DontResetFloatTimer
STZ $1594,x
BRA SkipThisNextCheck

DontResetFloatTimer:

LDA $1594,x
CMP #59
BNE DontResetTimer
SkipThisNextCheck:
LDA #$1A  ;was 4F
STA $1DF9 ; was 1DFC

DEC $1534,x
LDA $1534,x
CMP #$FF
BNE DontResetOnes
LDA #$09
STA $1534,x
DEC $151C,x
LDA $151C,x
CMP #$FF
BEQ LoseGame
DontResetOnes:
DontResetTimer:
SkipToGraphics:
JSR Graphics
RTS


LoseGame:
;LDA $71
;CMP #$09
;BNE DontKill
;JSL $00F606
;JSR Graphics
;RTS

STZ $1534,x
STZ $151C,x
STZ $1DFA
STZ $14

LDA #$01
STA $160E,x

DontKill:
;JSR Graphics




MarioHasWon:

LDA $7FAB10,x
AND #$04
CMP #$04
BNE NoExtraBit
LDA #$06
STA $71
STZ $89
STZ $88

NoExtraBit:
INC $1594,x
LDA $1594,x
CMP #119
BNE DontResetFloatTimer2
STZ $1594,x
DontResetFloatTimer2:

LDA $187B,x
BNE StopHandStuff
LDA $14
CMP #39
BCS DontTransfer
STA $1528,x
DontTransfer:

LDA #$01
STA $160E,x

LDA $14
CMP #20
BCC DontDrawTheItem
LDA #$02
STA $160E,x
DontDrawTheItem:
LDA $14
CMP #$80
BCC DrawTheItem54
LDA #$01
STA $160E,x
DrawTheItem54:

LDA $14
CMP #$80
BNE DontCreateItem
JSR SpawnNorm
LDA #$01
STA $187B,x
DontCreateItem:

StopHandStuff:
STZ $1411
JSR Graphics

RTS

;=====================================================;
;Spawn a normal sprite at Mario's position	      ;
;=====================================================;


SpawnNorm:
JSL $02A9DE

BMI Return
LDA $19
BEQ SpawnMushroom
LDA #$75
STA $00
BRA DoneSpawning
SpawnMushroom:
LDA #$74
STA $00
DoneSpawning:
PHX
TYX
LDA #$01
STA $14C8,x
LDA $00
STA $9E,x
JSL $07F7D2

STX $0D

;Store it to Mario's position.
PLX
LDA $E4,x
STA $00
LDA $14E0,x
STA $01
LDA $D8,x
STA $02
LDA $14D4,x
STA $03

REP #$20
LDA $00
CLC
ADC #$001F
STA $00

LDA $02
CLC
ADC #$0007
STA $02
SEP #$20


PHX
LDX $0D
LDA $00
STA $E4,x		;lox
LDA $01
STA $14E0,x		;hix
LDA $02
STA $D8,x		;loy
LDA $03
STA $14D4,x		;hiy

PLX
Return:
RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:
;LDA $1540,x
;BNE QuitGraphics

		
                    JSL !GetDrawInfo       ;A:87BF X:0007 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1102 VC:066 00 FL:665
			LDA $1534,x
			STA $09
			LDA $151C,x
			STA $0A

			LDA $1594,x
			STA $0F
			SEC
			SBC #$05	
			BPL NoResetCapeFloat
			CLC
			ADC #119
			NoResetCapeFloat:	
			STA $0B

			PHX			;Draw the timer first because the OAM is silly that way.
			
			LDA $00
			CLC
			ADC #$04
			STA $0300,y

			LDA $01
			CLC
			ADC #$07
			PHX
			LDX $0F
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y
			
			LDX $0A
			LDA TimerTiles,x
			STA $0302,y
	
			LDA #$3B
			STA $0303,y

			INY #4
			
			LDA $00
			CLC
			ADC #$0D
			STA $0300,y

			LDA $01
			CLC
			ADC #$07
			PHX
			LDX $0F
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y
			
			LDX $09
			LDA TimerTiles,x
			STA $0302,y
	
			LDA #$3B
			STA $0303,y
			PLX


			INY #4


			LDA $00			;Draw the smile/frown
			CLC
			ADC #$08
			STA $0300,y

			LDA $01
			CLC
			ADC #25
			PHX
			LDX $0F
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y
			
			LDA $7FAB10,x
			AND #$04
			CMP #$04
			BEQ DrawSmile
			LDA $160E,x
			BEQ DrawSmile
			LDA #$E8
			BRA WeveDrawnTheFrown
			DrawSmile: 
			LDA #$E6
WeveDrawnTheFrown:
			STA $0302,y

			
			LDA #$33
			STA $0303,y
			
			INY #4
		

			PHX
			LDX #$09
Loop:			CPX #$FF
			BEQ EndGraphics

			LDA $00
			CLC
			ADC XOffsets,x
			STA $0300,y




			STA $7FFFFF
			CPX #$02
			BCS IsNotCape
IsCape:
			LDA $01
			CLC
			ADC YOffsets,x
			PHX
			LDX $0B
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y
			BRA DoneWithCapeDrawing
IsNotCape:
			LDA $01
			CLC
			ADC YOffsets,x
			PHX
			LDX $0F
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y
		DoneWithCapeDrawing:	



			LDA Tiles,x
			STA $0302,y
			
			LDA Properties,x
			STA $0303,y
			
			DEX
			INY #4
			BRA Loop
            EndGraphics:
			;Draw the hand and the item it holds

			PLX


LDA $7FAB10,x
AND #$04
CMP #$04
BEQ NoExtraBit3

			LDA $1528,x
			STA $0E
			LDA $160E,x

			PHX
			;LDX #$00
		        ;CMP #$00
			;BEQ DontSetX
			LDX $0E
			LSR
			DontSetX:


			LDA $00
			CLC
			ADC HandX,x
			STA $0300,y

			LDA $01
			CLC
			ADC HandY,x
			PHX
			LDX $0F
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y

			LDA #$EA
			STA $0302,y

			LDA #$33
			STA $0303,y

INY #4
			STX $0D
			PLX
			LDA $160E,x
			PHX
			LDX $0D
			CMP #$02
			BNE DoneDrawingItem
			LDA $00
			CLC
			ADC HandX,x
			STA $0300,y

			LDA $01
			CLC
			ADC HandY,x
			PHX
			LDX $0F
			CLC
			ADC SineTable,x
			PLX
			STA $0301,y

			LDA $19
			BEQ DrawMushroom
			LDA #$26
			STA $0302,y
			LDA #$3A
			STA $0303,y
			BRA DoneDrawingItem
			DrawMushroom:
			LDA #$24
			STA $0302,y
			LDA #$38
			STA $0303,y
			DoneDrawingItem:

		PLX
NoExtraBit3:
                    LDY #$02                ; \ we've already set 460 so use FF
                    LDA #$0E                ; | A = number of tiles drawn - 1
                    JSL $01B7B3             ; / don't draw if offscreen
QuitGraphics:       RTS                     ; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Shatter Vial Routine.  This is ripped directly from the Yoshi's Egg sprite.  Yay for no documentation! =D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ShatterVial:

CODE_01F7C8:	JSR.w IsSprOffScreen      
CODE_01F7CB:	BNE Return01F82C          
CODE_01F7CD:	LDA $E4,x
CODE_01F7CF:	STA $00                   
CODE_01F7D1:	LDA $D8,x   
CODE_01F7D3:	STA $02                   
CODE_01F7D5:	LDA.w $14D4,x
CODE_01F7D8:	STA $03                   
CODE_01F7DA:	PHX                       
CODE_01F7DB:	LDY.b #$03                
CODE_01F7DD:	LDX.b #$0B                
CODE_01F7DF:	LDA.w $17F0,X             
CODE_01F7E2:	BEQ CODE_01F7F4           
CODE_01F7E4:	DEX                       
CODE_01F7E5:	BPL CODE_01F7DF           
CODE_01F7E7:	DEC.w $185D               
CODE_01F7EA:	BPL CODE_01F7F1           
CODE_01F7EC:	LDA.b #$0B                
CODE_01F7EE:	STA.w $185D               
CODE_01F7F1:	LDX.w $185D               
CODE_01F7F4:	LDA.b #$03                
CODE_01F7F6:	STA.w $17F0,X             
CODE_01F7F9:	LDA $00                   
CODE_01F7FB:	CLC                       
CODE_01F7FC:	ADC.w DATA_01F831,Y       
CODE_01F7FF:	STA.w $1808,X             
CODE_01F802:	LDA $02                   
CODE_01F804:	CLC                       
CODE_01F805:	ADC.w DATA_01F82D,Y       
CODE_01F808:	STA.w $17FC,X             
CODE_01F80B:	LDA $03                   
CODE_01F80D:	STA.w $1814,X             
CODE_01F810:	LDA.w DATA_01F835,Y       
CODE_01F813:	STA.w $1820,X             
CODE_01F816:	LDA.w DATA_01F839,Y       
CODE_01F819:	STA.w $182C,X             
CODE_01F81C:	TYA                       
CODE_01F81D:	ASL                       
CODE_01F81E:	ASL                       
CODE_01F81F:	ASL                       
CODE_01F820:	ASL                       
CODE_01F821:	ASL                       
CODE_01F822:	ASL                       
CODE_01F823:	ORA.b #$28                
CODE_01F825:	STA.w $1850,X             
CODE_01F828:	DEY                       
CODE_01F829:	BPL CODE_01F7E4           
CODE_01F82B:	PLX                       
Return01F82C:	RTS                       ; Return 


IsSprOffScreen:	LDA.w $15A0,X ; \ A = Current sprite is offscreen 
CODE_0180CE:	ORA.w $186C,X ; /  
Return0180D1:	RTS                       ; Return 



DATA_01F82D:                      db $00,$00,$08,$08

DATA_01F831:                      db $00,$08,$00,$08

DATA_01F835:                      db $E8,$E8,$F4,$F4

DATA_01F839:                      db $FA,$06,$FD,$03



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; routines below can be shared by all sprites.  they are ripped from original
; SMW and poorly documented
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/sprite check - shared
; Y = 1 if mario left of sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B817                ; Y = 1 if contact

SUB_GET_DIR:         LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0F                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL16             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL16:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B829 - vertical mario/sprite position check - shared
; Y = 1 if mario below sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B829 

SUB_VERT_POS:        LDY #$00               ;A:25A1 X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizCHC:0130 VC:085 00 FL:924
                    LDA $96                ;A:25A1 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdiZCHC:0146 VC:085 00 FL:924
                    SEC                    ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0170 VC:085 00 FL:924
                    SBC $D8,x              ;A:2546 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizCHC:0184 VC:085 00 FL:924
                    STA $0F                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0214 VC:085 00 FL:924
                    LDA $97                ;A:25D6 X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0238 VC:085 00 FL:924
                    SBC $14D4,x            ;A:2501 X:0007 Y:0000 D:0000 DB:03 S:01EA P:envMXdizcHC:0262 VC:085 00 FL:924
                    BPL LABEL11            ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0294 VC:085 00 FL:924
                    INY                    ;A:25FF X:0007 Y:0000 D:0000 DB:03 S:01EA P:eNvMXdizcHC:0310 VC:085 00 FL:924
LABEL11:             RTS                    ;A:25FF X:0007 Y:0001 D:0000 DB:03 S:01EA P:envMXdizcHC:0324 VC:085 00 FL:924


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; $B817 - horizontal mario/sprite check - shared
; Y = 1 if mario left of sprite??
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ;org $03B817        

SUB_HORZ_POS:        LDY #$00                ;A:25D0 X:0006 Y:0001 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1020 VC:097 00 FL:31642
                    LDA $94                 ;A:25D0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZCHC:1036 VC:097 00 FL:31642
                    SEC                     ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1060 VC:097 00 FL:31642
                    SBC $E4,x               ;A:25F0 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizCHC:1074 VC:097 00 FL:31642
                    STA $0F                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1104 VC:097 00 FL:31642
                    LDA $95                 ;A:25F4 X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1128 VC:097 00 FL:31642
                    SBC $14E0,x             ;A:2500 X:0006 Y:0000 D:0000 DB:03 S:01ED P:envMXdiZcHC:1152 VC:097 00 FL:31642
                    BPL LABEL17             ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1184 VC:097 00 FL:31642
                    INY                     ;A:25FF X:0006 Y:0000 D:0000 DB:03 S:01ED P:eNvMXdizcHC:1200 VC:097 00 FL:31642
LABEL17:             RTS                     ;A:25FF X:0006 Y:0001 D:0000 DB:03 S:01ED P:envMXdizcHC:1214 VC:097 00 FL:31642


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SUB_OFF_SCREEN
; This subroutine deals with sprites that have moved off screen
; It is adapted from the subroutine at $01AC0D
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SPR_T12:             db $40,$B0
SPR_T13:             db $01,$FF
SPR_T14:             db $30,$C0,$A0,$C0,$A0,$F0,$60,$90		;bank 1 sizes
		            db $30,$C0,$A0,$80,$A0,$40,$60,$B0		;bank 3 sizes
SPR_T15:             db $01,$FF,$01,$FF,$01,$FF,$01,$FF		;bank 1 sizes
					db $01,$FF,$01,$FF,$01,$00,$01,$FF		;bank 3 sizes

SUB_OFF_SCREEN_X1:   LDA #$02                ; \ entry point of routine determines value of $03
                    BRA STORE_03            ;  | (table entry to use on horizontal levels)
SUB_OFF_SCREEN_X2:   LDA #$04                ;  | 
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X3:   LDA #$06                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X4:   LDA #$08                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X5:   LDA #$0A                ;  |
                    BRA STORE_03            ;  |
SUB_OFF_SCREEN_X6:   LDA #$0C                ;  |
                    BRA STORE_03            ;  | OMG YOU FOUND THIS HIDDEN z0mg place!111 you win a cookie!
SUB_OFF_SCREEN_X7:   LDA #$0E                ;  |
STORE_03:			STA $03					;  |            
					BRA START_SUB			;  |
SUB_OFF_SCREEN_X0:   STZ $03					; /

START_SUB:           JSR SUB_IS_OFF_SCREEN   ; \ if sprite is not off screen, return
                    BEQ RETURN_35           ; /
                    LDA $5B                 ; \  goto VERTICAL_LEVEL if vertical level
                    AND #$01                ; |
                    BNE VERTICAL_LEVEL      ; /     
                    LDA $D8,x               ; \
                    CLC                     ; | 
                    ADC #$50                ; | if the sprite has gone off the bottom of the level...
                    LDA $14D4,x             ; | (if adding 0x50 to the sprite y position would make the high byte >= 2)
                    ADC #$00                ; | 
                    CMP #$02                ; | 
                    BPL ERASE_SPRITE        ; /    ...erase the sprite
                    LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0756 VC:176 00 FL:205
                    AND #$01                ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0780 VC:176 00 FL:205
                    ORA $03                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0796 VC:176 00 FL:205
                    STA $01                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0820 VC:176 00 FL:205
                    TAY                     ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0844 VC:176 00 FL:205
                    LDA $1A                 ;A:8A01 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizcHC:0858 VC:176 00 FL:205
                    CLC                     ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0882 VC:176 00 FL:205
                    ADC SPR_T14,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZcHC:0896 VC:176 00 FL:205
                    ROL $00                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizcHC:0928 VC:176 00 FL:205
                    CMP $E4,x               ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:0966 VC:176 00 FL:205
                    PHP                     ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:0996 VC:176 00 FL:205
                    LDA $1B                 ;A:8AC0 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizCHC:1018 VC:176 00 FL:205
                    LSR $00                 ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdiZCHC:1042 VC:176 00 FL:205
                    ADC SPR_T15,y           ;A:8A00 X:0009 Y:0001 D:0000 DB:01 S:01F0 P:envMXdizcHC:1080 VC:176 00 FL:205
                    PLP                     ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F0 P:eNvMXdizcHC:1112 VC:176 00 FL:205
                    SBC $14E0,x             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1140 VC:176 00 FL:205
                    STA $00                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1172 VC:176 00 FL:205
                    LSR $01                 ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:eNvMXdizCHC:1196 VC:176 00 FL:205
                    BCC SPR_L31             ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1234 VC:176 00 FL:205
                    EOR #$80                ;A:8AFF X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdiZCHC:1250 VC:176 00 FL:205
                    STA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1266 VC:176 00 FL:205
SPR_L31:             LDA $00                 ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1290 VC:176 00 FL:205
                    BPL RETURN_35           ;A:8A7F X:0009 Y:0001 D:0000 DB:01 S:01F1 P:envMXdizCHC:1314 VC:176 00 FL:205
ERASE_SPRITE:        LDA $14C8,x             ; \ if sprite status < 8, permanently erase sprite
                    CMP #$08                ; |
                    BCC KILL_SPRITE         ; /    
                    LDY $161A,x             ;A:FF08 X:0007 Y:0001 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1108 VC:059 00 FL:2878
                    CPY #$FF                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZCHC:1140 VC:059 00 FL:2878
                    BEQ KILL_SPRITE         ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1156 VC:059 00 FL:2878
                    LDA #$00                ;A:FF08 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdizcHC:1172 VC:059 00 FL:2878
                    STA $1938,y             ;A:FF00 X:0007 Y:0000 D:0000 DB:01 S:01F3 P:envMXdiZcHC:1188 VC:059 00 FL:2878
KILL_SPRITE:         STZ $14C8,x             ; erase sprite
RETURN_35:           RTS                     ; return

VERTICAL_LEVEL:      LDA $167A,x             ; \ if "process offscreen" flag is set, return
                    AND #$04                ; |
                    BNE RETURN_35           ; /
                    LDA $13                 ; \
                    LSR A                   ; | 
                    BCS RETURN_35           ; /
                    LDA $E4,x               ; \ 
                    CMP #$00                ;  | if the sprite has gone off the side of the level...
                    LDA $14E0,x             ;  |
                    SBC #$00                ;  |
                    CMP #$02                ;  |
                    BCS ERASE_SPRITE        ; /  ...erase the sprite
                    LDA $13                 ;A:0000 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:1218 VC:250 00 FL:5379
                    LSR A                   ;A:0016 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1242 VC:250 00 FL:5379
                    AND #$01                ;A:000B X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1256 VC:250 00 FL:5379
                    STA $01                 ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1272 VC:250 00 FL:5379
                    TAY                     ;A:0001 X:0009 Y:00E4 D:0000 DB:01 S:01F3 P:envMXdizcHC:1296 VC:250 00 FL:5379
                    LDA $1C                 ;A:001A X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0052 VC:251 00 FL:5379
                    CLC                     ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0076 VC:251 00 FL:5379
                    ADC SPR_T12,y           ;A:00BD X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0090 VC:251 00 FL:5379
                    ROL $00                 ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:enVMXdizCHC:0122 VC:251 00 FL:5379
                    CMP $D8,x               ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0160 VC:251 00 FL:5379
                    PHP                     ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0190 VC:251 00 FL:5379
                    LDA $001D             ;A:006D X:0009 Y:0001 D:0000 DB:01 S:01F2 P:eNVMXdizcHC:0212 VC:251 00 FL:5379
                    LSR $00                 ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdiZcHC:0244 VC:251 00 FL:5379
                    ADC SPR_T13,y           ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:enVMXdizCHC:0282 VC:251 00 FL:5379
                    PLP                     ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F2 P:envMXdiZCHC:0314 VC:251 00 FL:5379
                    SBC $14D4,x             ;A:0000 X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNVMXdizcHC:0342 VC:251 00 FL:5379
                    STA $00                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0374 VC:251 00 FL:5379
                    LDY $01                 ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0398 VC:251 00 FL:5379
                    BEQ SPR_L38             ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0422 VC:251 00 FL:5379
                    EOR #$80                ;A:00FF X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0438 VC:251 00 FL:5379
                    STA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0454 VC:251 00 FL:5379
SPR_L38:             LDA $00                 ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0478 VC:251 00 FL:5379
                    BPL RETURN_35           ;A:007F X:0009 Y:0001 D:0000 DB:01 S:01F3 P:envMXdizcHC:0502 VC:251 00 FL:5379
                    BMI ERASE_SPRITE        ;A:8AFF X:0002 Y:0000 D:0000 DB:01 S:01F3 P:eNvMXdizcHC:0704 VC:184 00 FL:5490

SUB_IS_OFF_SCREEN:   LDA $15A0,x             ; \ if sprite is on screen, accumulator = 0 
                    ORA $186C,x             ; |  
                    RTS                     ; / return