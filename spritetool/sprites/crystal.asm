incsrc subroutinedefs_xkas.asm

TILEMAP:	
	db $22,$26
	db $24,$28

print "INIT ", pc
	RTL

Return:
	RTS

print "MAIN ", pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL
	
Retun:
	RTS
	
Run:
	JSR GFX
	JSR SUB_OFF_SCREEN_X0
	LDA $14C8,x		;If the sprite is dead..
	CMP #$08
	BNE Return		;..return
	LDA $9D
	BNE Return		;Also return if sprites are locked.
	JSR SHOOTING_STARS	
	
	JSL $01A7DC
	BCC Return
	JSL $00F5B7
	
	RTS

XSPD:
	db $E0,$20,$E0,$20,$00,$20,$00,$E0		; x speed of fireballs ;; add more values if you
YSPD:
	db $E0,$E0,$20,$20,$20,$00,$E0,$00		; y speed of fireballs ;; spawn more fireballs!!

SHOOTING_STARS:
	LDA $163E,x
	BNE NOSPAWN
	JSR SPAWNM
	LDA #$50
	STA $163E,x
NOSPAWN:
	RTS

SPAWNM:	
	PHX		; jump to this label
	LDX #$07	; load times to go through loop -1
LOOPM:	
	JSR CHKFREE
	DEX
	BPL LOOPM
	PLX
	RTS
CHKFREE:	
	LDY #$07
LOOPFRE:	
	LDA $170B,Y
	BEQ SPAWNEX
	DEY
	BPL LOOPFRE
	RTS
SPAWNEX:
	LDA #$02	; extended sprite number
	STA $170B,y
	PHX
	LDX $15E9
	LDA $D8,X
	CLC
	ADC #$04	; y offset of fireballs
	STA $1715,Y
	LDA $14D4,X
	ADC #$00
	STA $1729,Y
	LDA $E4,X
	CLC
	ADC #$04	; x offset of fireballs
	STA $171F,Y
	LDA $14E0,X
	ADC #$00
	STA $1733,Y
	PLX
	LDA XSPD,X
	STA $1747,Y
	LDA YSPD,X
	STA $173D,Y
	RTS	

;Sprite Routines
	
XDISP:	
	db $00,$10
	db $00,$10
	
YDISP:	
	db $00,$00
	db $10,$10
	
GFX:
	JSL !GetDrawInfo
	LDA $157C,x
	STA $02
	
	LDA $15F6,x
	STA $04
	
	LDX #$00
	
OAM_Loop:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300,y
	
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301,y
	
	LDA TILEMAP,x
	STA $0302,y
	
	LDA $04
	ORA $64
	STA $0303,y
	
	INY
	INY
	INY
	INY
	
	INX
	CPX #$04
	BNE OAM_Loop
	
	LDX $15E9
	LDY #$02
	LDA #$03
	JSL $01B7B3
	RTS
;=================;
;SUB_HORZ_POS
;=================;

SUB_HORZ_POS:
	LDY #$00
	LDA $D1
	SEC
	SBC $E4,x
	STA $0F
	LDA $D2
	SBC $14E0,x
	BPL SPR_L16
	INY
SPR_L16:
	RTS

;=================;
;SUB_VERT_POS
;=================;

SUB_VERT_POS:
	LDY #$00
	LDA $D3
	SEC
	SBC $D8,x
	STA $0F
	LDA $D4
	SBC $14D4,x
	BPL SPR_L11
	INY
SPR_L11:
	RTS

;=================;
;SUB_OFF_SCREEN
;=================;

SPR_T12:
	db $40,$B0
SPR_T13:
	db $01,$FF
SPR_T14:
	db $30,$C0,$A0,$C0,$A0,$F0,$60,$90
	db $30,$C0,$A0,$80,$A0,$40,$60,$B0
SPR_T15:
	db $01,$FF,$01,$FF,$01,$FF,$01,$FF
	db $01,$FF,$01,$FF,$01,$00,$01,$FF

SUB_OFF_SCREEN_X1:
	LDA #$02
	BRA STORE_03
SUB_OFF_SCREEN_X2:
	LDA #$04
	BRA STORE_03
SUB_OFF_SCREEN_X3:
	LDA #$06
	BRA STORE_03
SUB_OFF_SCREEN_X4:
	LDA #$08
	BRA STORE_03
SUB_OFF_SCREEN_X5:
	LDA #$0A
	BRA STORE_03
SUB_OFF_SCREEN_X6:
	LDA #$0C
	BRA STORE_03
SUB_OFF_SCREEN_X7:
	LDA #$0E
STORE_03:
	STA $03
	BRA START_SUB
SUB_OFF_SCREEN_X0:
	STZ $03

START_SUB:
	JSR SUB_IS_OFF_SCREEN
	BEQ RETURN_35
	LDA $5B
	AND #$01
	BNE VERTICAL_LEVEL
	LDA $D8,x
	CLC
	ADC #$50
	LDA $14D4,x
	ADC #$00
	CMP #$02
	BPL ERASE_SPRITE
	LDA $167A,x
	AND #$04
	BNE RETURN_35
	LDA $13
	AND #$01
	ORA $03
	STA $01
	TAY
	LDA $1A
	CLC
	ADC SPR_T14,y
	ROL $00
	CMP $E4,x
	PHP
	LDA $1B
	LSR $00
	ADC SPR_T15,y
	PLP
	SBC $14E0,x
	STA $00
	LSR $01
	BCC SPR_L31
	EOR #$80
	STA $00
SPR_L31:
	LDA $00
	BPL RETURN_35
	ERASE_SPRITE:
	LDA $14C8,x
	CMP #$08
	BCC KILL_SPRITE
	LDY $161A,x
	CPY #$FF
	BEQ KILL_SPRITE
	LDA #$00
	STA $1938,y
KILL_SPRITE:
	STZ $14C8,x
RETURN_35:
	RTS

VERTICAL_LEVEL:
	LDA $167A,x
	AND #$04
	BNE RETURN_35
	LDA $13
	LSR A
	BCS RETURN_35
	LDA $E4,x
	CMP #$00
	LDA $14E0,x
	SBC #$00
	CMP #$02
	BCS ERASE_SPRITE
	LDA $13
	LSR A
	AND #$01
	STA $01
	TAY
	LDA $1C
	CLC
	ADC SPR_T12,y
	ROL $00
	CMP $D8,x
	PHP
	LDA $001D
	LSR $00
	ADC SPR_T13,y
	PLP
	SBC $14D4,x
	STA $00
	LDY $01
	BEQ SPR_L38
	EOR #$80
	STA $00
SPR_L38:
	LDA $00
	BPL RETURN_35
	BMI ERASE_SPRITE

SUB_IS_OFF_SCREEN:
	LDA $15A0,x
	ORA $186C,x
	RTS

