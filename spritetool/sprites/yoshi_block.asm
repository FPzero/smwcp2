;==========
;New Sprite
;==========
incsrc subroutinedefs_xkas.asm
!Flag = $7C

print "INIT ", pc
	RTL

Return:
	RTS

print "MAIN ", pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
	RTL
	
Run:
	JSR GFX

	JSR SUB_OFF_SCREEN_X0
	LDA $14C8,x		;If the sprite is dead..
	CMP #$08
	BNE Return		;..return
	LDA $9D
	BNE Return		;Also return if sprites are locked.
	
	JSL $01A7DC
	BCC Return
	
	LDA $C2,x
	PHX
	ASL A
	TAX
	JMP (Ptr,x)
	
Ptr:
	dw Hide	;00
	dw Wait	;01
	dw Spawn	;02

Spawn:
	PLX
	LDA #$10
	STA $1DF9
	JSR DrawSmoke
	JSR SpawnYoshi
	LDA #$01
	STA !Flag
	STZ $14C8,x
	RTS
Wait:
	PLX
	LDA $1540,x
	BEQ NextState	
	LDA #$FF
	STA $78
	STZ $7D
	STZ $7B
	LDA $E4,x
	STA $94
	LDA $14E0,x
	STA $95
	LDA $D8,x
	STA $96
	LDA $14D4,x
	STA $97
	LDA $13
	AND #$03
	TAY
	BNE +
	LDA #$22
	STA $1DF9	
	LDA Movement,y
	STA $B6,x
	STZ $AA,x
	JSL $01802A
+
	RTS
NextState:
	INC $C2,x
	RTS
Hide:
	PLX
	LDA #$FF
	STA $78
	STZ $7D
	STZ $7B
	LDA #$12
	STA $1540,x	; time to wait
	INC $C2,x
	RTS

Movement: 
	db $02,$FE,$03,$FC

SpawnYoshi:
	LDA $186C,x
	BNE EndSpawn
	JSL $02A9DE
	BMI EndSpawn
	LDA #$01
	STA $14C8,y
	LDA #$35
	STA $009E,y
	LDA $94
	STA $00E4,y
	LDA $95
	STA $14E0,y
	LDA $96
	STA $00D8,y
	LDA $97
	STA $14D4,y
	PHX
	TYX
	JSL $07F7D2
	PLX
EndSpawn:
	RTS
	
DrawSmoke:
	LDY #$03
-
	LDA $17C0,y
	BEQ +
	DEY
	BPL -
	RTS
+
	LDA #$01
	STA $17C0,y
	LDA #$1C     
	STA $17CC,y
	LDA $D8,x
	STA $17C4,y
	LDA $E4,x
	STA $17C8,y
	RTS

;Sprite Routines
  
TILEMAP:	
	db $80,$82
	db $A0,$A2
	
XDISP:	
	db $00,$10
	db $00,$10
	
YDISP:	
	db $00,$00
	db $10,$10
	
GFX:
	JSL !GetDrawInfo
	LDA $157C,x
	STA $02
	
	LDA $15F6,x
	STA $04
	
	LDX #$00
	
OAM_Loop:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300,y
	
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301,y
	
	LDA TILEMAP,x
	STA $0302,y
	
	LDA $04
	ORA $64
	STA $0303,y
	
	INY
	INY
	INY
	INY
	
	INX
	CPX #$04
	BNE OAM_Loop
	
	LDX $15E9
	LDY #$02
	LDA #$03
	JSL $01B7B3
	RTS
;=================;
;SUB_HORZ_POS
;=================;

SUB_HORZ_POS:
	LDY #$00
	LDA $D1
	SEC
	SBC $E4,x
	STA $0F
	LDA $D2
	SBC $14E0,x
	BPL SPR_L16
	INY
SPR_L16:
	RTS

;=================;
;SUB_VERT_POS
;=================;

SUB_VERT_POS:
	LDY #$00
	LDA $D3
	SEC
	SBC $D8,x
	STA $0F
	LDA $D4
	SBC $14D4,x
	BPL SPR_L11
	INY
SPR_L11:
	RTS

;=================;
;SUB_OFF_SCREEN
;=================;

SPR_T12:
	db $40,$B0
SPR_T13:
	db $01,$FF
SPR_T14:
	db $30,$C0,$A0,$C0,$A0,$F0,$60,$90
	db $30,$C0,$A0,$80,$A0,$40,$60,$B0
SPR_T15:
	db $01,$FF,$01,$FF,$01,$FF,$01,$FF
	db $01,$FF,$01,$FF,$01,$00,$01,$FF

SUB_OFF_SCREEN_X1:
	LDA #$02
	BRA STORE_03
SUB_OFF_SCREEN_X2:
	LDA #$04
	BRA STORE_03
SUB_OFF_SCREEN_X3:
	LDA #$06
	BRA STORE_03
SUB_OFF_SCREEN_X4:
	LDA #$08
	BRA STORE_03
SUB_OFF_SCREEN_X5:
	LDA #$0A
	BRA STORE_03
SUB_OFF_SCREEN_X6:
	LDA #$0C
	BRA STORE_03
SUB_OFF_SCREEN_X7:
	LDA #$0E
STORE_03:
	STA $03
	BRA START_SUB
SUB_OFF_SCREEN_X0:
	STZ $03

START_SUB:
	JSR SUB_IS_OFF_SCREEN
	BEQ RETURN_35
	LDA $5B
	AND #$01
	BNE VERTICAL_LEVEL
	LDA $D8,x
	CLC
	ADC #$50
	LDA $14D4,x
	ADC #$00
	CMP #$02
	BPL ERASE_SPRITE
	LDA $167A,x
	AND #$04
	BNE RETURN_35
	LDA $13
	AND #$01
	ORA $03
	STA $01
	TAY
	LDA $1A
	CLC
	ADC SPR_T14,y
	ROL $00
	CMP $E4,x
	PHP
	LDA $1B
	LSR $00
	ADC SPR_T15,y
	PLP
	SBC $14E0,x
	STA $00
	LSR $01
	BCC SPR_L31
	EOR #$80
	STA $00
SPR_L31:
	LDA $00
	BPL RETURN_35
	ERASE_SPRITE:
	LDA $14C8,x
	CMP #$08
	BCC KILL_SPRITE
	LDY $161A,x
	CPY #$FF
	BEQ KILL_SPRITE
	LDA #$00
	STA $1938,y
KILL_SPRITE:
	STZ $14C8,x
RETURN_35:
	RTS

VERTICAL_LEVEL:
	LDA $167A,x
	AND #$04
	BNE RETURN_35
	LDA $13
	LSR A
	BCS RETURN_35
	LDA $E4,x
	CMP #$00
	LDA $14E0,x
	SBC #$00
	CMP #$02
	BCS ERASE_SPRITE
	LDA $13
	LSR A
	AND #$01
	STA $01
	TAY
	LDA $1C
	CLC
	ADC SPR_T12,y
	ROL $00
	CMP $D8,x
	PHP
	LDA $001D
	LSR $00
	ADC SPR_T13,y
	PLP
	SBC $14D4,x
	STA $00
	LDY $01
	BEQ SPR_L38
	EOR #$80
	STA $00
SPR_L38:
	LDA $00
	BPL RETURN_35
	BMI ERASE_SPRITE

SUB_IS_OFF_SCREEN:
	LDA $15A0,x
	ORA $186C,x
	RTS

