incsrc subroutinedefs_xkas.asm

!BulletsToShoot = $20	;$30 bullets until the thing disappears in a puff of smoke
!ShootInterval = $2C	;shoot interval thing
!Tile = $29		;16x16 Tile of blaster (SP2)
!SecondPage = $01	;$01 if the GFX is in the 2nd page

!Sprite = $6B		;which CUSTOM sprite to spawn when shooting

!BulletCounter = $1510	;ram of bullet counter
!ShootTimer = $163E	;ram of shoot interval

print "INIT ",pc

		LDA #!BulletsToShoot
		STA !BulletCounter,x

		LDA #!ShootInterval
		STA !ShootTimer,x
		RTL

print "MAIN ",pc

		PHB
		PHK
		PLB
		JSR Main
		PLB
		RTL

X_OFFSET:	db $03^$FF,$03
X_OFFSET2:	db $FF,$00

Offscreenhandle:	STZ $03
			PHB
			LDA #$01
			PHA
			PLB
			PEA $801F
			JML $81AC33


Return2:	RTS
Continue2:	JMP Continue

Show_Smoke:	LDY #$03		; \ find a free slot to display effect
.FINDFREE	LDA $17C0,y		;  |
		BEQ .FOUNDONE		;  |
		DEY			;  |
		BPL .FINDFREE		;  |
		RTS			; / return if no slots open

.FOUNDONE	LDA #$01		; \ set effect to smoke
		STA $17C0,y		; /

		LDA $D8,x		; \ set y pos of smoke
		STA $17C4,y		; /

		LDA $E4,x		; \ set x pos of smoke
		STA $17C8,y		; /

		LDA #$18		; \ set smoke duration
		STA $17CC,y		; /
		RTS

Show_Smoke2:	LDY #$03		; \ find a free slot to display effect
.FINDFREE	LDA $17C0,y		;  |
		BEQ .FOUNDONE		;  |
		DEY			;  |
		BPL .FINDFREE		;  |
		RTS			; / return if no slots open

.FOUNDONE	LDA $157C,x
		STA $00

		LDA #$01		; \ set effect to smoke
		STA $17C0,y		; /

		LDA $D8,x		; \ set y pos of smoke
		STA $17C4,y		; /

		PHY
		LDY $0000
		LDA Xoffset,y
		CLC
		ADC $E4,x		; \ set x pos of smoke
		PLY
		STA $17C8,y		; /

		LDA #$18		; \ set smoke duration
		STA $17CC,y		; /
		RTS

Xoffset:	db $F8,$08

Main:		LDA !BulletCounter,x
		BNE +
		STZ $14C8,x
		JSR Show_Smoke
		RTS

+		JSR Graphics
		LDA $9D		;if locked, return
		BNE Return2

		JSL Offscreenhandle

		LDA $AA,x
		BNE Fire

		JSL $01A7DC
		BCC Continue2

		BIT $15
		BVC Continue2

		LDA #$0B		; Sprite status = Carried
		STA $14C8,x

		LDA $76
		STA $157C,x

Fire:		LDA !ShootTimer,x
		BNE Return

		LDA $15A0,x
		ORA $186C,x
		ORA $15D0,x
		BNE Return

		JSL $02A9DE
		BMI Return

		LDA #$08
		STA $14C8,y

		PHX
		TYX
		LDA #!Sprite
		STA $7FAB9E,x
		PLX

		PHY
		LDA $157C,x
		TAY
		LDA $E4,x
		CLC
		ADC X_OFFSET,y
		PLY
		STA $00E4,y

		PHY
		LDA $157C,x
		TAY
		LDA $14E0,x
		ADC X_OFFSET2,y
		PLY
		STA $14E0,y

		LDA $D8,x
		STA $00D8,y
		LDA $14D4,x
		STA $14D4,y

		PHX
		TYX
		JSL $07F7D2
		JSL $0187A7
		LDA #$08
		STA $7FAB10,x
		PLX

		LDA $157C,x
		STA $157C,y

		LDA #$09
		STA $1DFC

		JSR Show_Smoke2

		DEC !BulletCounter,x
		LDA #!ShootInterval
		STA !ShootTimer,x
Return:		RTS	

Continue:	LDA #!ShootInterval
		STA !ShootTimer,x
		RTS

Graphics:	JSL !GetDrawInfo
		LDA $00
		STA $0300,y
		LDA $01
		STA $0301,y
		LDA #!Tile
		STA $0302,y


		LDA $14
		AND #$07
		ASL
		STA $00

		LDA $157C,x
		CLC
		ROR A : ROR A : ROR A
		ORA $64		; add level priority
anus:		ORA $00
		ORA #!SecondPage
		STA $0303,y

		LDY #$02		;16x16
		LDA #$00		;One tile
		JSL $01B7B3

		RTS

