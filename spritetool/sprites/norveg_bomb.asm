;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

incsrc subroutinedefs_xkas.asm

!State = $1534
!StateTimer = $163E

!sprite_y_pos_hi = $14D4
!sprite_y_pos_lo = $D8

!sprite_x_pos_hi = $14E0
!sprite_x_pos_lo = $E4

!sprite_x_speed = $B6
!sprite_y_speed = $AA

!sprite_x_acc_bits = $14F8
!sprite_y_acc_bits = $14EC

!hurt_mario = $80F5B7

!position = $1510 ;0-4, drop position, index for various tables. Set during spawn, never again.

                    print "INIT ",pc
					STZ !State,x
					LDA #$E0
					STA $AA,x
					STZ $B6,x
                    LDA #$26
                    STA $1DF9
					RTL
	                print "MAIN ",pc			
                    PHB
                    PHK				
                    PLB
                    JSR SPRITE_ROUTINE			;Jump to the routine to keep organized
                    PLB
                    RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

falling_x_pos:
    dw $0118,$0138,$01B8,$01D8
falling_y_pos:
    dw $0060,$0020,$0020,$0060

falling_y_pos_max:
    dw $0132,$00F0,$00F0,$0132

!rising_min_y_pos = #$0060

!rising_speed = #$C0
!falling_speed = #$40
!falling_wait_time = #$50

SPRITE_ROUTINE:	  	
					LDA $9D
				    beq +
                        jmp return
                    +
					LDA !State,x
					bne +
                        jmp .rising
                    + 
                    cmp #$01
                    bne +
                        jmp .falling_init
                    + 
                    cmp #$02
                    bne +
                        jmp .wait_to_fall
                    +
                    cmp #$03
                    bne +
                        jmp .falling
                    +
                    cmp #$04
                    bne +
                        jmp .exploding
                    +
                    .kill
                        stz $14C8,x
                        rts
                    .rising
                        lda !rising_speed
                        sta !sprite_y_speed,x
                        ;JSL $818022
                        JSL $81801A
                        lda !sprite_y_pos_hi,x
                        xba
                        lda !sprite_y_pos_lo,x
                        rep #$20
                        cmp !rising_min_y_pos
                        bpl +
                            inc !State,x
                        +
                        sep #$20
                        JSR SUB_GFX                 ;Draw the graphics
                        jmp return
                    .falling_init
                        inc !State,x
                        lda !falling_wait_time
                        sta !StateTimer,x
                        phx
                        lda !position,x
                        asl
                        tax
                        rep #$20
                        lda falling_x_pos,x
                        sta $00
                        lda falling_y_pos,x
                        plx
                        sep #$20
                        sta !sprite_y_pos_lo,x
                        xba
                        sta !sprite_y_pos_hi,x
                        lda $00
                        sta !sprite_x_pos_lo,x
                        lda $01
                        sta !sprite_x_pos_hi,x
                        jmp return
                    .wait_to_fall
                        jsr blink_arrow
                        lda !StateTimer,x
                        bne +
                            inc !State,x
                        +
                        rts
                    .falling
                        lda !falling_speed
                        sta !sprite_y_speed,x
                        JSL $81801A
                        JSR SUB_GFX
                        lda !position,x
                        asl
                        phx
                        tax
                        rep #$20
                        lda falling_y_pos_max,x
                        sta $00
                        sep #$20
                        plx
                        lda !sprite_y_pos_hi,x
                        xba
                        lda !sprite_y_pos_lo,x
                        rep #$20
                        cmp $00
                        sep #$20
                        bcc +
                            JSR DrawSmoke
                            lda #$20
                            sta !StateTimer,x
                            inc !State,x
                            LDA #$09
                            STA $1DFC
                        +
                        jmp return

					.exploding
						JSL $81A7DC
                        bcc +
                            jsl !hurt_mario
                        +
						LDA !StateTimer,x
						bne +
						  inc !State,x
						+
						jmp return
					return:
                    RTS							;End the routine
					
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Tiles:
db $C0,$C1,$C2,$C3

SUB_GFX:            JSL !GetDrawInfo      	;Get all the drawing info, duh
					LDA $157C,x
					STA $05
                    PHX
					LDA $14
					LSR
					LSR
					AND #$03
					TAX
					
					
					LDA $00
					STA $0300,y
					
					LDA $01
					STA $0301,y
					
					LDA Tiles,x
					STA $0302,y
					PHX
					LDX $15E9
					LDA $15F6,x
					PLX
					ORA $64
					;ORA #$30
					PHX
					LDX $05
					BNE +
					ORA #$40
					+
					PLX
					STA $0303,y
;					INY
;					INY
;					INY
;					INY
					PLX
					LDA #$00
					LDY #$00					;|they were all 16x16 tiles
					JSL $81B7B3					;/and then draw em
                    ldy $15EA,x
                    iny #4
                    tya
                    sta $15EA,x
					EndIt:
					RTS
	

DrawSmoke:          LDY #$03                ; \ find a free slot to display effect
FINDFREE:           LDA $17C0,y             ;  |
                    BEQ FOUNDONE            ;  |
                    DEY                     ;  |
                    BPL FINDFREE            ;  |
                    RTS                     ; / return if no slots open

FOUNDONE:           LDA #$02                ; \ set effect graphic to contact graphic
                    STA $17C0,y             ; /
                    LDA #$20                ; \ set time to show smoke
                    STA $17CC,y             ; /
                    LDA $D8,x               ; \ smoke y position = generator y position
                    STA $17C4,y             ; /
                    LDA $E4,x               ; \ load generator x position and store it for later
                    STA $17C8,y             ; /
                    RTS	

!arrow_tile = #$0D
!arrow_props = #$85
arrow_x_pos:
    db $18,$38,$C4,$DB
arrow_y_pos:
    db $A0,$50,$50,$A0
blink_arrow:
    lda $14
    lsr #4
    and #$01
    bne .return
    ldy $15EA,x
    phx
    lda !position,x
    tax
    lda arrow_x_pos,x
    sta $0300,y
    lda arrow_y_pos,x
    sta $0301,y
    plx
    lda !arrow_tile
    sta $0302,y
    lda !arrow_props
    ora $64
    sta $0303,y

    phy
    tya
    lsr #2
    tay
    lda #$02
    sta $0460,y
    ply

    ldy $15EA,x
    iny #4
    tya
    sta $15EA,x
    .return
    rts