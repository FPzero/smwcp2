;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
;
; Shared Subroutines 1.0, by imamelia
;
; This patch inserts a lot of common subroutines into your ROM so that you can
; use them without having to copy-paste them.  Some examples are GetDrawInfo,
; SubOffscreen, and the Map16 tile-generating subroutine.
;
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

header
lorom

incsrc subroutinedefs_xkas.asm

!FreespaceU = $81E2B0  ;was 81F500 but that's in the middle of an opcode

org !FreespaceU
; This is the starting address for the subroutines.  All "JSL [insert subroutine here]" commands should
; be JSLing to this bank.

autoclean JML GetDrawInfoMain		; GetDrawInfo: sets up some variables for a sprite GFX routine
autoclean JML SubOffscreenX0		; SubOffscreen: checks if a sprite is offscreen, sets offscreen flag if necessary
autoclean JML SubHorizPosMain		; SubHor(i)zPos: checks which horizontal side of a sprite the player is on; stores the horizontal distance between them to $0F
autoclean JML SubVertPosMain		; SubVertPos: checks which vertical side of a sprite the player is on; stores the vertical distance between them to $0E
autoclean JML SubSetMap16Main		; Sub(l)SetMap16: changes one Map16 tile to another
autoclean JML SubOffscreenX3		; SubOffscreen: checks if a sprite is offscreen, sets offscreen flag if necessary
autoclean JML GetRand

warnpc $01E2CF

freecode

incsrc subroutinemaincode.asm

; org $81F4FF
	; LDA $00					;$01F4FF	|
	; CLC					;$01F501	|
	; ADC.w $185E				;$01F502	|
	; BPL CODE_01F509				;$01F505	|
	; DEC $01					;$01F507	|
; CODE_01F509:
	; CLC
	; ADC $D8,X				;$01F50A	|
	; STA.w $D8,Y				;$01F50C	|
	; LDA.w $14D4,X				;$01F50F	|
	; ADC $01					;$01F512	|
	; STA.w $14D4,Y				;$01F514	|
	; LDA.b #$00				;$01F517	|
	; STA.w $AA,Y				;$01F519	|
	; STA.w $B6,Y				;$01F51C	|
	; INC A					;$01F51F	|
	; STA.w $15D0,Y				;$01F520	|
	; RTS					;$01F523	|
