#SPC
{
	#author "Slash Man"
	#comment "Factory theme 1"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 t59 w160



#0 @0 $ED $41 $E9 y12 l1 q7f v132 $EF $18 $6C $6C 
$F1 $04 $5A $01 
$F2 $1A $28 $29 
(1)[o2a+^^^] o3c^^^ *
/
[o2g^^^a+^^^]4
[r]8
@1 $ED $67 $EA y9
v170 o3a+^f^a+^>c^
@0 $ED $41 $E9 y12 v132 (1)1



#1 @0 $ED $41 $E9 y8 l1 q7f v123
(2)[o3a+^^^] o4c^^^ *
/
[o3g^^^a+^^^]4
[r]8
@1 $ED $67 $EA y11 
v170 o3f^c^f^g^
@0 $ED $41 $E9 y8 v122 (2)1



#2 @4 $ED $1A $E1 v135 l8 q7f y10 r8 $EF $18 $4C $5C ^8^4^2 
[r1]7(3)[o1a+a+4a+4ef16g16g+a+a+4a+4>d<eg
a+a+a+a+4ef+g+a+a+4a+4a+4r]
/
t59(4)[o1gg4g4c+d16e16fgg4g4a+d+f+
gggg4c+d+fgg4g4.r4](3)(4)(3)(4)(3)(4)
[r1]4[o1a+a+a+a+a+ff+g+a+a+a+a+a+ff+g+
ffffaaaa>ccccd+d+d+d+]4(3)1



#3 v165 y10 l2 q7f
[r1]12
/ 
@0 $ED $0A $E0 p8,21
[r1]12
[o3a+>c+c{d+4c+4<a+4}>c<f1^4r4
a>c<b>{d4c4e4}dfe^4r4]2
[r1]4
[o3a+16>c+16f1^8d+4c+4<a+8>c16c+16
c<f1^4r4a+8>c+8f1
d+4{c+4<a+4>c+4}cf]
o5 c1 *
o5 $ED $4D $EB c1^1^
r1^1^



#5 q7f 
[r1]12
/
[r1]20 l1 @1 $ED $67 $EA v182 y9
o3a+^^^g^^^ [r1]4
@3 $ED $5E $C3 v156 l4 
[o3a+r2^a+ra+g
ar1^2^a+r2^
a+r{a+ga+}ar>drar2^]2 [r1]4



#6 q7f 
[r1]12
/
[r1]20 l1 @1 $ED $67 $EA v182 y11
o3f^^^d^^^
[r1]4 @3 $ED $5E $C3 v156 l4
[o4c+r2^c+rc+<a+
>cr1^2^c+r2^
c+r{c+<a+>c+}crfr>cr2^]2 [r1]4



#4 q7f v160 l8 y10
[r1]8
(6)[o4@21c4]16
/
(6)13r4.@21c@29c@29c16@29c16
(5)[@21c@21c16@21c16@29c@21c4@21c@29c@21c
@21c@21c@29c@21c4@21c@29c@29c
@21c@21c16@21c16@29c@21c4@21c@29c@21c
@21c@21c@29c@21c4@21c@29c@21c16@29c16]6
@21c@21c16@21c16@29c@21c4@21c@29c@21c
@21c@21c@29c@21c4@21c@29c@29c
@21c@21c16@21c16@29c@21c4@21c@29c@21c
@21c@21c@29c@21c@29c@21c@29c16@29c16@29c16@29c16 (5)4(6)16



#7 q7f y10 l16

(7)[o4 v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c 
              @23  $ED $1C $E0 c 
              @23  $ED $1C $E0 c 
              @23  $ED $1C $E0 c 
              @23  $ED $1C $E0 c 
    v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c

    v110 y8   @23  $ED $1C $E0 c 
              @23  $ED $1C $E0 c 
              @23  $ED $1C $E0 c  
              @23  $ED $1C $E0 c
 
    v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c  
              @23  $ED $1C $E0 c  
              @23  $ED $1C $E0 c 
    
v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c  
              @23  $ED $1C $E0 c  
              @23  $ED $1C $E0 c 
    
v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c 
    v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c   
              @23  $ED $1C $E0 c  
              @23  $ED $1C $E0 c 
    v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c
    
v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c 
    v180 y12  @23  $ED $7F $E0 c 
    v110 y8   @23  $ED $1C $E0 c]6
/
(7)26                

#amk=1
