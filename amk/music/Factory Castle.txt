;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;ASMWCP2 - Factory Castle
;;;;;Composed/Ported by: Slash Man
;;;;;Song Duration: 1:36
;;;;;Insert Size:1679 bytes
;;;;;AddMusicM required! (not)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#SPC
{
	#author "Slash Man"
	#comment "Factory castle theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
	"CCymbal.brr"
}

#0 t56 w160

$EF $9D $36 $36 
$F1 $04 $37 $01

#0 @9 $ED $2E $F0 q6f y11 l16 v163
[r1]6
/
[r1]6
(1)[o4c+8c+8c+8d+ef+8f+8f+ed+8
f+ed+c+d+ef+ed+c+d+ef+ed+e
c+8c+8c+8d+ef+8f+8f+ed+8
f+ed+c+d+ef+ed+c+d+ef+ed+e
c+8c+c+c+8e8c+8c+8f+8e8
d+8d+d+d+8f+8d+8d+8g+8f+8
e8eee8g+8e8e8a8g+8
f+ed+c+ed+c+<b>d+c+<ba>c+<bag+]2
(2)[o4c+8c+c+c+4r8c+8d+d+8^
c+8c+c+c+4r8c+8<ab8^
>c+8c+c+c+4r8c+8d+d+8^
c+8c+c+c+4r8f+8f+f+8^]
(13)[o4c+8c+c+c+4r8c+8d+d+8^
c+8c+c+c+4r8c+8<ab8^
>c+8c+c+c+4r8c+8d+d+8^
c+8c+c+c+4r8c+8c+c+8^]
(3)[o4c+8c+8e4r8c+d+ed+c+<b4.>
r8^c+d+ef+d+ef+g+
d+8d+8f+4r8d+f+g+ag+c+4.
r8^f+ed+c+d+ef+g+
c+8c+c+c+8e8c+8c+8f+8e8
d+8d+d+d+8f+8d+8d+8g+8f+8
e8eee8g+8e8e8a8g+8
f+ed+c+ed+c+<b>d+c+<ba>c+<bag+]2
(4)[o4c+8c+8c+8d+ef+8f+8f+ed+8
f+ed+c+d+ef+ed+c+d+ef+ed+e]2
(5)[o4c+4r1..]

#1 @8 $ED $7E $ED v239 y10 l8 q7f
(6)[o2c+c+16c+16c+ec+c+f+e]6
/
(6)10(7)[c+c+16c+16c+ec+c+f+e
d+d+16d+16d+f+d+d+g+f+
ee16e16eg+eeag+
f+f+16f+16ee16e16d+d+16d+16c+c+16c+16]
(6)5(8)[d+d+16d+16d+f+d+d+g+f+
ee16e16eg+eeag+]r1
(6)12(7)1(6)5(8)1
[c+2r2
c+c+16c+16c+ec+c+f+e
c+c+16c+16c+ec+c+f+e
c+c+16c+16c+ec+c+f+e]2

#2 q7f y10 l8 v170
[r1]3
o4@29c32c^32@21c.c@29c16c16@21c@29cc16c16
(11)[@21c.c16@29c@21crc@29c@21c
@21c.c16@29c@21crc@29c@21c16@29c16]
/
@21c.c16@29c@21crc@29c@21c
@21c.c16@29c@21crc@29cc16c16
(9)[@21c.c16@29c@21crc@29c@21c
@21c.c16@29c@21crc@29c@21c16@29c16
@21c.c16@29c@21crc@29c@21c
@21c.c16@29c@21crc@29cc16c16]4
(10)[@21c.c16@29c@21crc@29c@21c
@21c.c16@29c@21crc@29c@21c16@29c16
@21c.c16@29c@21crc@29c@21c
@29c32c^32@21c.c@29c16c16@21c@29cc16c16]
(9)(10)
(9)3(10)2(11)1

#3 q7f
[r1]6
/
l1 r^
@0 $ED $12 $EC y12 v103 
o5 c+^ $ED $12 $EA c+^^[r]7 
l4 @1 $ED $37 $E4 y10 v194
[o3c+2^ec+2^<b]2
o3c+2^ed+2^f+
e2^g+f+ed+c+
[r1]8[c+1d+1e1f+1
c+2^ed+2^f+
e2^g+]f+ed+c+ *
[r1]7

#5 l1 q7f
[r]6
/
l1 r^ @0 $ED $12 $EC v105 y10
o4e^ $ED $12 $EA f+^^
[r]15 $ED $5A $E3 v137 l4
$EF $BD $36 $36 
[o4c+ed+f+c+ed+<b
>c+ed+f+] o4e8c+8d+8<b8>c+8e8d+8f+8
* o4c+ed+<b
[o4c+2^f+e2^c+
d+2^g+f+1
c+2^ed+2^f+e2^g+] f+1
* 
$ED $5A $EA
f+1^1[r1]6

#7 l1 q7f
[r]6
/
l1 r^ @0 $ED $12 $EC v105 y9 
o4c+^ $ED $12 $EA d+^^
@9 $ED $3E $F0 q6f y11 l4 v153
[r1]23
[o4e8e8g+r..d+.r2^8.
d+8d+8f+r..e.r2^8.
r1^1^1^1]2
[r1]20

#6 q7f y9 l16
 @22 v160c @22v120c @22c @22c @22c @22c 

v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c
(12)[ @22 v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c]3
@0 ("CCymbal.brr", $02) h0 v119 $ED $7A $F1 o5c2 @22v120 o4 @22c@22c@22c@22c v160@22c v120@22c@22c@22c
(12)1
/
$EF $9D $36 $36 
(12)2[@0 ("CCymbal.brr", $02) h0 v119 $ED $7B $F1 o5c2  v120 o4 @22c@22c@22c@22c v160@22c v120@22c@22c@22c
v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c
v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c
v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c@22c@22c v160@22c v120@22c@22c@22c]13

#4 @9 $ED $3C $F0 q6f y9 l16 v119
[r1]6
/
[r1]6r8
(1)2(2)(13)(3)2(4)2(5)                

#amk=1
