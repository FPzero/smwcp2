#SPC
{
	#author "Buster Beetle"
	#comment "Oriental theme 3"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0
t76
w160

$EF $2D $33 $33
$F1 $04 $4C $01
$F2 $32 $4D $4D

#0
o3
@14
$ED $0F $52
v225
p5,10
[<e8r8  a8r8b8r8g8r8
e8r8a8r8b8r8a8r8
e8r8a8r8b8r8g8r8
>d8r8c8r8<b8r8a8r8]10

#1
o2
@8
$ED $0F $52
v255
p10,25
(6)[e8d8e8g8e8d8r4
e8d8e8g8e8r4^8
e8d8e8g8e8d8r4
c8r8c8r8c8r8d8r8]2
e8d8e8g8e8g8r4
e8d8e8g8e8r4^8
e8d8e8g8b8g8r4
g8f+8g8e8g8r8f+8r8
e8d8e8r8d8r4^8
e8d8e8r8d8r8g8r8
e8d8e8g8e8r8d8r8
c8r8c8r8b8r8a8r8
(7)[e8d8e8d8e8r8g8r8]1
e8d8e8d8e8r8d8r8
(7)1
c8r8c8r8c8r8d8r8(7)1
e8d8e8d8e8r8d8r8(7)1
b8r8a8r8g8r8f+8r8
(9)[g8r8e8r8g8r8e8d8
g8r8e8r8g8r8g8r8]1
(8)[g8r8f+8r8g8r8a8r8]2
(9)1(8)2(6)2

#2
o4
@4
$ED $5D $04
p15,30
v255
[r1]16
<b2^4a8g8
f+2^4e8f+8
g2e2
d2c2
b2^4a8g8
f+2^4>d8c8
<b2e4c8e8
b8r8a8r8g8r8f+8r8
[g4f+4g2]2
b2a2
g2f+2
[g4f+4g2]2
b2a4g4
f+4e4f+4g4
e1^2
r1^1^2
g2e2
d1
g2e2
d2e2

#3
o4
@0
v235
$ED $5A $C0
p16,20
[e2^4d4]2
e2g2
a2b2
e2^4d4
e2^4d4
g2a2
a1
e2^4d4
e2^4d4
b2a2
g4f+4e4d4
e2^4d4
e2^4a4
a1
f+1
e2^4d4
e2^4d4
e2g2
a2b2
e2^4d4
e2^4d4
g2a2
a1
e2^4d4
e2^4d4
e4g4f+4d4
g4a4a4d4
e2^4d4
e2^4d4
g2>d4c4
<b2f+2
g2^4f+4
g2^4f+4
g2^4>d4
c4<b4a4f+4
g2^4f+4
g2^4f+4
e1
f+1

#5
o4
v255
@13
$ED $7F $EF
p20,25
[r1]8
e2^4g4
f+4e4d2
e2f+2
g2a2
g2f+2
g2a2
e1
f+2d2
e2^4g4
f+4e4d2
e2f+2
g2a2
g2^4b4
a4g4f+2
g2e4g4
a2f+2
g2^4f+4
g2f+4g4
b1
a1
g2^4f+4
g2f+4g4
>d1
<a2g4f+4
e1
f+4g4a4f+4
e1
f+2d2
e1
f+2d2
e2^4d8e8
f+4g4a4f+4

#4
o3
$ED $7F $EF
v255
@21c16r2^4^8^16
@21c16r2^4^8^16
@21c16r1^2^4^8^16
@21c16r2^4^8^16
@21c16r4^8^16@21c16r4^8^16
@21c16r4^8^16@21c16r8^16@10d16r8^16
@21c16r4^8^16@21c16r4^8^16

(1)[@21c16r8^16@21c16r8^16@21c16r8^16@21c16r8^16]5
@21c16r8^16@21c16r8^16@10d16r8^16@10d16r8^16(1)1

@21c16r8^16@21c16r16@21c16r16@10d16r16@21c16r16@10d16r8^16
@21c16r8^16@10d16r8^16@21c16r8^16@10d16r8^16
@21c16r8^16@10d16r8^16@21c16r8^16@10d16r16@21c16r16
(2)[@21c16r16@21c16r16@10d16r16@21c16r16]4
(3)[@21c16r8@21c16@10d16r16@21c16r16@21c16r8@21c16@10d16r16@21c16r16
@21c16r8@21c16@10d16r16@21c16r16@21c16r8@21c16@10d16r16@10d16r16]1
[@21c16r8@21c16@10d16r16@21c16r16@21c16r8@21c16@10d16r16@21c16r16
@21c16r8@21c16@10d16r16@21c16r16@21c16r8@21c16@10d16@10d16@10d16@10d16]2
(3)1
(2)4
@21c16r8^16@10d16r8^16@21c16r8^16@10d16r8^16
@21c16r8^16@10d16r8^16@21c16r8^16@10d16r16@21c16r16
(4)[@21c16r8^16@21c16r8^16@10d16r8^16@21c16r8^16]1
@21c16r8^16@21c16r16@21c16r16@10d16r16@21c16r16@10d16r8^16
(4)
@21c16r8^16@21c16r8^16@10d16r4^8^16
(4)2
@21c16r8^16@21c16r8^16@21c16r8^16@21c16r8^16
@21c16r4^8^16@21c16r1;
                

#amk=1
