#amk 2

#samples {
	#default
	#AMM
	#sky_and_dream
}

#pad $01E5

#option TempoImmunity
#option NoLoop

#0 t46 w255
$EF $FC $0F $0F
$F4 $02
@3v200q5a
o5e=4d=4e=4r=36
$F4 $03
(1)[$F3 $20 $04 $ED $80 $2C q7fo4a+12b24a+12b24g12d24r12f24c12c+24d24c+24c24
<a+12g24a+12r24g24r6f4]
r=127

#1
r4
v230@8$ED $0F $EE
o1g12>g24<g12>g24<f12>f24r12f24<e12>e24<e24>d24<a24
d12a+24d12f24g=40$DD $10 $18 c $DE $10 $0F $30>g4.
r=127

#2
r6v210
y12q77@28c24q7b@28c24
$F4 $03
y10q7f@21c8
$F4 $03
y12@28c8
$F4 $03
y10@21c12
$F4 $03
y12@28c24
$F4 $03
y10@21c24q7b@21c24
$F4 $03
y12@28c24
$F4 $03
y10q7f@21c8
$F4 $03
y12@28c12
$F4 $03
y10@21c8@21c24
$F4 $03
y12@28c24q7b@28c24q77@28c24q7f@28c12
$F4 $03
y10@21c8
$F4 $03
y12@28c24q7a@28c24y10q78@28c24y8@28c24y12@28c24y10q79o2a+24y8q7bf+24y12q7dd24y10q7f<a+=127

#3
r4v130@13$ED $0F $ED
o4g4a8.r16g16r16g12f8r24f8<b8r12$DE $10 $12 $34>f4.
r=127

#4
r4y11v130@13$ED $0F $ED
d4c8.r16c16r16c12d8r24d8<g8r12$DE $10 $12 $34b4.
r=127

#5
r4y8v140
q7a@23c8@23c12@23c24@23c12@23c24q77@23c24q73@23c24q7a@23c8@23c8@23c24
@23c12@23c24q75@23c12@23c24q7a@23c12q75@23c8q7a@23c24q7bo3g24q7cd24q7d<a24q7ee24q7f<b=127

#6
r4^12
y9v100
(1)
r=127

#7
r4y9v130@13$ED $0F $ED
o3b4f8.r16e16r16e12a+8r24a+8d8r12$DE $10 $12 $34d4.
r=127