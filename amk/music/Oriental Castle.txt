#SPC
{
	#author "AxemJinx"
	#comment "Oriental castle theme"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 w160 t44
$EF $FF $15 $15
$F1 $04 $6C $00
#0 @14 v235 o5 l16
(1)[c+<g+a+f8d+f8r4
<g+8r8]

o5c+<g+a+f8g+a+>c+
d+8f8d+8<g+8
>
(1)

o5c+<g+a+f8a+>d+f
g+4r4

c+<g+a+f8d+f8r4
<g+a+r8

o5c+<g+a+f8g+a+>c+
d+8f8d+8<a+8
>
c+<g+a+f8d+f8r8^
<a+g+a+r8

o5c+<g+a+f8a+>d+f
g+4a+4
[r8fr8^a+r8^
g+f+c+<g+>d+a+r8>c+r8^<a+r8^
g+rd+f+a+d+r8fr8^g+r8^
a+g+f+d+a+>c+r8<g+r8^a+ra+32g+32f+32d+32<a+32>c+32f+32a+32>c+32<a+32f+32g+32a+32f+32g+32d+32
]2
<[a+8>d+c+d+8<f+8
a+g+a+8>c+<a+>c+8<a+8>d+c+d+8<f+8
g+>c+d+f+g+f+d+<g+
a+8>d+c+d+8<f+8
a+g+a+8>c+<a+>c+8<g+>c+d+f+<a+>d+g+f+
d+32c+32<a+32f+32g+32>c+32f+32a+32g+32d+32f+32c+32d+32<a+32>c+32<g+32
]2

a+>c+d+g+c+d+g+a+
<g+a+>c+d+<a+>c+d+g+
<
f+g+a+>c+<d+f+g+a+
f+g+a+>f+fc+<a+g+
<
a+>c+d+f+<f+g+a+>c+
c+d+f+a+<g+a+>c+d+

d+f+g+a+<a+>c+d+f+
c+f+>c+f+g+f+d+c+
<
a+>c+d+g+c+d+g+a+
<g+a+>c+d+<a+>c+d+g+
<
d+f+g+>c+<c+d+f+g+
f+a+>d+f+g+f+d+c+

o3a+>c+d+f+<f+g+a+>c+
<g+a+>c+d+f+g+a+>c+

d+<a+g+d+>c+<g+f+c+

[>c+<g+a+f8d+f8]2

>c+<g+a+f8g+a+>c+
f4r4

#1 @21 v255 l16 o4

[dr8.dr8.
dr8ddr8.
dr8.dr8.
dr8.dr8.
dr8.dr8.
dr8ddr8.
dr8.dr8.
dr8.drdr]2
[dr8.drdr
dr8ddr8.

drdr8.dr
drdr8ddr

dr8ddrdr
dr8ddr8.

drdr8.dr
dr8ddrdr]2
[dr8dr8dr
dddr8.dr

dr8ddrdr8.
dr8ddr
dr8dr8dr
dddr8.dr

dr8ddrdr
dr8ddrdr]2
[dr4^dr
dr4..

dr4^dr
dr8.dr8.
]3
drdddrdr
dr8.drdr

drdddrdr
dr4.d

dr4.d
dr8.dr8.

dr8ddr8.

#2 v0 l16 o5 (@29, $03) $ED $0E $6A

e4v225er4..
<cr4..
>er4^
er8.<cr

o2gr8.o5er4..
<cr4..
>er4
<ccr8.cr

o2gr8.o5er4..
<cr4..
>er4^
er8.<cr

o2gr8.o5er4..
<cr4..
>er8.
o2go4ccro2go4ccc

[o2gr8.o4cr8.
o2gr8.o4cr8.
o2gro4cr8.cr
o2gro5d+r8.d+r

o2gr8.o4cr8.
o2gr8.o4cr8.
o2gro4cr8ccr
o2gro5d+ro2go5d+d+d+
]2
[o2gro5d+r<cr>d+ro2gr8.o4cr>d+r
o2gro5d+r<cr>d+d+o2gro4cr8.cr

o2gro5d+r<cr>d+r
o2gr8.o4cr>d+d+
o2gro5d+r<cr>d+ro2go5d+<cro2gro4cc]2
o2[gr1^2^4^8^]3 
[gr8.]2 
gr4.. [
gr8.]2 
gr1^8.
o5d+r4..
<cr8.

#3 @23 v0 l16 o3
[r1]4
g8
v255gr8^gr8^
gr8ggr
r8
gr8^gr8^
gr8grg

r8gr8^gr8^
gr8ggr
r8
gr8^gr8^
grgggr
[r8
grgr8^
gr8ggr8^
r8
gr8^gr
gr8grgr8
r8
grgr8^
gr8ggr8^
r8
ggrgrg
grgggr8^]2
[r8
grgr8^
gr8ggr8^
r8
gr8^gr
gr8grgr8
r8
grgr8^
gr8ggr8^
r8
ggrgrg
grgggrgg]2
gr2...[r1]9

#7 @13 v240 l16 o5
[r1]4
a+2^4
>c4

<g+4^8d+g+
f4>d+8c+8

<a+2^4
c+4

g+4^8f+g+

a+8g+a+>f4r2
[r1]3
r2^8
<d+4^8

r8f+4^8
r8g+4^8

r8a+4^8
r8>c+4^8

r8d+4^8
r8f+4^8

r8g+8f+8d+8
<a+4>d+4

c+4<g+8f+g+
a+8d+8d+8f+a+

>c+8<g+8g+8g+8
a+4>d+4

c+4<f+8f+g+
a+8>c+8<f+8g+8

d+4d+4
a+4>d+4

c+4<g+8f+g+
a+8d+8d+8f+a+

>c+8<g+8g+8g+8
a+4>d+4

c+4d+8<f+g+
a+8>c+8<f+8g+8

d+4d+4
>f+4f8d+8

f4c+4
d+4<a+4

>c+4<f+8g+8
a+4f+4

g+4a+8>c+8
d+4<a+4

>c+4<g+4
>f+4f8d+8

f4<a+4
>c+4d+4

<f+4d+8f+8
a+8>c+8<g+8a+8

d+4d+8f+8
g+a+>c+d+<f+g+a+>c+

[<ff+g+>c+]4 c+4r2^4

#5 @4 v170 l16 o2

[r1]4
r8
d+8f+8f+8
g+8g+8a+8a+8

r8c+8f+8f+8
g+8g+8a+8>c+8

r8d+8c+8<a+8
f+8f+8a+8g+8

r8d+8f+8g+8
>c+8<a+8>c+8f+8<
[d+4g+4
f+4d+4

a+4^8c+8
f+4<a+4

>a+4f+4
g+4c+4

d+8f+8g+8a+8
d+8<a+8>d+4]2
d+4g+4
c+4g+8f+8

d+4^8g+8
c+4<g+4

>d+4f+4
c+4g+8f+8

d+4^8<g+8
a+8>c+8f+8g+8

d+4g+4
c+4g+8f+8

d+4^8g+8
c+4<g+4

>d+4f+4
c+4a+8g+8

f+4d+4
>c+4<g+4
[r1]10

#4 @17 v210 l16 o3

[r1]8
[d+8a+8a+8d+8
f+4g+4

d+>c+<a+g+f+8d+8
a+8c+8r8c+8

d+8a+8a+8f+8
g+4>c+4

d+<a+>c+<g+a+f+c+g+
d+4^16r16d+8]2
(2)[d+8a+8a+8f+8
c+8g+8g+4

d+8a+8a+8d+8
f+4g+4

d+8a+8a+8f+8
g+a+>c+d+<g+a+>c+d+]

<a+8f+8f+8c+8
>c+4<g+4

(2)
<c+4d+4
f+g+8a+>c+8d+8
[r1]10                

#amk=1
